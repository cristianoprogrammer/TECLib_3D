/******************************************************************************/
/*
 * TEC3DVertex.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DVertex_hpp
#define TEC3DVertex_hpp

#include <stdio.h>

#include "TEC3DFoundation.h"

#ifdef __cplusplus


/******************************************************************************/
// TEC3DVertex
/******************************************************************************/
namespace TEC3D {
	
	/*........................................................................*/
	// Vertex
	/*........................................................................*/
	struct _Vertex {
		
		Vector3f pos;
		Vector3f norm;
		Vector2f texC;
		Vector4f boneId;
		Vector4f boneWeights;
		
		_Vertex() {
			
			boneWeights.x = 0.0f;
			boneWeights.y = 0.0f;
			boneWeights.z = 0.0f;
			boneWeights.w = 0.0f;
			
			boneId.x = 0.0f;
			boneId.y = 0.0f;
			boneId.z = 0.0f;
			boneId.w = 0.0f;
			
		}
		
		void AddBoneData(uint BoneID, float Weight)
		{
			if (boneWeights.x == 0.0) {
				boneWeights.x = Weight;
				boneId.x	= BoneID;
				return;
			 }
			 
			 if (boneWeights.y == 0.0) {
				boneWeights.y = Weight;
				boneId.y	= BoneID;
				return;
			 }
			 
			 if (boneWeights.z == 0.0) {
				boneWeights.z = Weight;
				boneId.z	= BoneID;
				return;
			 }
			 
			 if (boneWeights.w == 0.0) {
				boneWeights.w = Weight;
				boneId.w	= BoneID;
				return;
			 }

		}

		
	};
	
	typedef _Vertex Vertex;
	
}

#endif

#endif /* TEC3DVertex_hpp */
