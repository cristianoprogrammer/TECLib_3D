//
//  AppDelegate.h
//  TECLib_3D
//
//  Created by Cristiano Douglas on 25/01/17.
//  Copyright © 2017 Tecvidya Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

