/*.............................................................................*/
/*
 * TEC3DShader.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 31/01/17.
 *
 */
/*.............................................................................*/

#include "TEC3DShader.hpp"

using namespace TEC3D;

/******************************************************************************/
// TEC3DShader
/******************************************************************************/

/*.............................................................................*/
// Constructor's
/*.............................................................................*/
Shader::Shader(std::string vertex, std::string fragment) {
	this->fragmentShader = fragment;
	this->vertexShader = vertex;
	if (!this->compileVertexShader()) return;
	if (!this->compileFragmentShader()) return;
	if (!this->setupShaderProgram()) return;
}

Shader::Shader() {
	
}

/*.............................................................................*/
// Set Fragment Shader
/*.............................................................................*/
GLint Shader::setFragment(std::string fragment) {
	this->fragmentShader = fragment;
	return this->compileFragmentShader();
}

/*.............................................................................*/
// Set Vertex Shader
/*.............................................................................*/
GLint Shader::setVertex(std::string vertex) {
	this->vertexShader = vertex;
	return this->compileVertexShader();
}

/*.............................................................................*/
// User shader program
/*.............................................................................*/
void Shader::useProgram() {
	if (this->success) {
		char message[512];
		glValidateProgram(this->program);
		GLint result = 0;
		glGetProgramiv(this->program, GL_VALIDATE_STATUS, &result);
		if (result == GL_FALSE) {
			glGetProgramInfoLog(this->program, 512, NULL, message);
			printf("*************************************************\n");
			printf("TEC3DShader<Validade> compile message:\n");
			printf("-------------------------------------------------\n");
			printf("%s\n",message);
			printf("*************************************************\n");
		}
		glUseProgram(this->program);
	}
}

/*.............................................................................*/
// Setup shader program
/*.............................................................................*/
GLint Shader::setupShaderProgram() {
	
	this->program = glCreateProgram();
	glAttachShader(this->program, this->fragmentId);
	glAttachShader(this->program, this->vertexId);
	
	glBindAttribLocation(this->program, 0, "vertexPosition");
	glBindAttribLocation(this->program, 1, "normal");
	glBindAttribLocation(this->program, 2, "texCoord");
	glBindAttribLocation(this->program, 3, "bonesId");
	glBindAttribLocation(this->program, 4, "bonesWeight");
	
	glLinkProgram(this->program);
	glGetProgramiv(this->program, GL_LINK_STATUS, &this->success);
	char message[512];
	if (this->success == GL_FALSE) {
		glGetProgramInfoLog(this->program, 512, NULL, message);
#ifdef __APPLE__
		printf("*************************************************\n");
		printf("TEC3DShader<Program> compile message:\n");
		printf("-------------------------------------------------\n");
		printf("%s\n",message);
		printf("*************************************************\n");
#endif
	} else {
		//glDeleteShader(this->vertexId);
		//glDeleteShader(this->fragmentId);
	}
	return this->program;
}

/*.............................................................................*/
// Compile the vertex shader
/*.............................................................................*/
GLint Shader::compileVertexShader() {
	//Vertex
	const GLchar* vertexShaderCode = this->vertexShader.c_str();
	this->vertexId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(this->vertexId, 1, &vertexShaderCode, NULL);
	glCompileShader(this->vertexId);
	glGetShaderiv(this->vertexId, GL_COMPILE_STATUS, &this->compileSuccess);
	char message[512];
	if (this->compileSuccess == GL_FALSE) {
		glGetShaderInfoLog(this->vertexId, 512, NULL, message);
		this->vertexCompileMessage = std::string(message);
#ifdef __APPLE__
		printf("*************************************************\n");
		printf("TEC3DShader<Vertex> compile message:\n");
		printf("-------------------------------------------------\n");
		printf("%s\n",message);
		printf("*************************************************\n");
#endif
	}
	return this->compileSuccess;
}

/*.............................................................................*/
// Compile the fragment shader
/*.............................................................................*/
GLint Shader::compileFragmentShader() {
	// Fragment
	const GLchar* fragmentShaderCode = this->fragmentShader.c_str();
	this->fragmentId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(this->fragmentId, 1, &fragmentShaderCode, NULL);
	glCompileShader(this->fragmentId);
	glGetShaderiv(this->fragmentId, GL_COMPILE_STATUS, &this->compileSuccess);
	char message[512];
	if (this->compileSuccess == GL_FALSE) {
		glGetShaderInfoLog(this->fragmentId, 512, NULL, message);
		this->fragmentCompileMessage = std::string(message);
#ifdef __APPLE__
		printf("*************************************************\n");
		printf("TEC3DShader<Fragment> compile message:\n");
		printf("-------------------------------------------------\n");
		printf("%s\n",message);
		printf("*************************************************\n");
#endif
	}
	return this->compileSuccess;
}

/*.............................................................................*/
// Get Vertex Shader id
/*.............................................................................*/
GLuint Shader::getVertexShader() {
	return this->vertexId;
}

/*.............................................................................*/
// Get Fragment Shader id
/*.............................................................................*/
GLuint Shader::getFragmentShader() {
	return this->fragmentId;
}

/******************************************************************************/
