/******************************************************************************/
/*
 * tec3d_surface_frag.frag
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 15/03/17.
 *
 */
/******************************************************************************/

// Default float precision
precision highp float;

// Ambient Light
uniform	highp vec3	ambientColor;

// Material Opacity
uniform	float		opacity;

// Textures
uniform	sampler2D	ambient_texture;

// Vertex Shader properties
varying highp vec2	TexCoord;

/******************************************************************************/
// main
/******************************************************************************/
void main()
{

	highp vec4 ambiTex2D = texture2D(ambient_texture,TexCoord);
	highp vec4 ambient	= vec4(ambientColor,opacity);
	if (ambiTex2D.rgb != vec3(0.0,0.0,0.0)) {
		ambient *= ambiTex2D;
	}
	
	highp vec4 fragmentColor = vec4(ambiTex2D.rgb,1.0);
	gl_FragColor = fragmentColor;
}
/******************************************************************************/
