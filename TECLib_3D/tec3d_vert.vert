/******************************************************************************/
/*
 * tec3d_vert.vert
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 31/01/17.
 *
 */
/******************************************************************************/
attribute vec3	vertexPosition;
attribute vec3	normal;
attribute vec2	texCoord;
attribute vec4	bonesId;
attribute vec4	bonesWeight;


uniform mat4	projectionMatrix;
uniform mat4	modelViewMatrix;
uniform mat4	normalMatrix;
uniform mat4	bones[100];

uniform vec3	lightColor;
varying vec2	TexCoord;
varying vec3	norm;
varying vec3	lightDir;
varying vec3	camDir;
varying vec3	weight;

/******************************************************************************/
// main
/******************************************************************************/
void main()
{
	
	mat4 boneTransform = bones[int(bonesId.x)] * bonesWeight.x;
	boneTransform += bones[int(bonesId.y)] * bonesWeight.y;
	boneTransform += bones[int(bonesId.z)] * bonesWeight.z;
	boneTransform += bones[int(bonesId.w)] * bonesWeight.w;
	
	
	// Position
	vec3 camPos		= vec3(0.0, 0.0, 1.0);
	vec3 lightPos	= vec3(0.0, 25.0, 25.0);
	vec4 animPos	= boneTransform*vec4(vertexPosition,1.0);
	vec4 pos		= projectionMatrix*modelViewMatrix*animPos;
	gl_Position		= pos;
	
//	vec3	normal = vec3(0.0,0.0,0.0);
//	vec2	texCoord = vec2(0.0, 0.0);
	
	// Light
	mat3 norm3  =  mat3(normalMatrix);
	norm		= normalize(normal * norm3);
	lightDir	= normalize(lightPos - pos.xyz);
	camDir		= normalize(camPos - vertexPosition);
	weight		= bonesWeight.xyz;
	
	TexCoord	= texCoord;
	
}
	
