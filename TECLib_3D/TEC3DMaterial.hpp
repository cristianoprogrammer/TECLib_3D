/******************************************************************************/
/*
 * TEC3DMaterial.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 13/02/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DMaterial_hpp
#define TEC3DMaterial_hpp

#include "TEC3DBase.h"
#include "TEC3DFoundation.h"

/******************************************************************************/
// TEC3DMaterial
/******************************************************************************/
namespace TEC3D {
	
	class Material {
	
	/*........................................................................*/
	// Public properties
	/*........................................................................*/
	public:
		
		// Name
		std::string name;
		
		// Wireframe
		GLuint wireframe;
		
		// Opacity
		GLfloat opacity;
		
		// Ambient
		Color3f ambientColor;
		GLfloat	ambientLightStrength;
		
		// Diffuse
		Color3f diffuseColor;
		GLfloat diffuseStrength;
		
		// Specular
		Color3f specularColor;
		GLfloat specularStrength;
		
		
	public:
		Material();
		
	};
	
	
}

/******************************************************************************/

#endif /* TEC3DMaterial_hpp */
