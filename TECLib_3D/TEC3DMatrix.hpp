/******************************************************************************/
/*
 * TEC3DMatrix.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 2/02/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DMatrix_hpp
#define TEC3DMatrix_hpp

#include "TEC3DBase.h"
#include "TEC3DFoundation.h"

#define tec3d_pi			(3.1415927f)
#define tec3d_radians(t)	((GLfloat)(tec3d_pi/180)*t)

/******************************************************************************/
// TEC3DMatrix
/******************************************************************************/
namespace TEC3D {
	
	/**************************************************************************/
	// Matrix4x4
	/**************************************************************************/
	class Matrix4x4 {
	
	public:
		
		Matrix4x4(GLfloat a0, GLfloat a1, GLfloat a2, GLfloat a3,
				  GLfloat b0, GLfloat b1, GLfloat b2, GLfloat b3,
				  GLfloat c0, GLfloat c1, GLfloat c2, GLfloat c3,
				  GLfloat d0, GLfloat d1, GLfloat d2, GLfloat d3);
		
		Matrix4x4(GLfloat matrix[4][4]);
		
		Matrix4x4();
		
	public:
		
		// Used matrix
		GLfloat m[4][4] =
			{{ 1, 0, 0, 0},
			{ 0, 1, 0, 0},
			{ 0, 0, 1, 0},
			{ 0, 0, 0, 1}};
		
		// Matrix identity
		GLfloat identityMatrix[4][4] =
			{{ 1, 0, 0, 0},
			{ 0, 1, 0, 0},
			{ 0, 0, 1, 0},
			{ 0, 0, 0, 1}};
		
		
	public:
	
		GLvoid identity();
		
		GLvoid rotate(GLfloat x, GLfloat y, GLfloat z);
		GLvoid rotate(Vector3f vector);
		GLvoid rotateX(GLfloat theta);
		GLvoid rotateY(GLfloat theta);
		GLvoid rotateZ(GLfloat theta);
		
		
		GLvoid scale(GLfloat x, GLfloat y, GLfloat z);
		GLvoid scale(Vector3f vector);
		GLvoid scaleX(GLfloat scale);
		GLvoid scaleY(GLfloat scale);
		GLvoid scaleZ(GLfloat scale);
		
		GLvoid translate(GLfloat x, GLfloat y, GLfloat z);
		GLvoid translate(Vector3f vector);
		GLvoid translateX(GLfloat value);
		GLvoid translateY(GLfloat value);
		GLvoid translateZ(GLfloat value);
		
	};
	
	
	
}


#endif /* TEC3DMatrix_hpp */
