/******************************************************************************/
/*
 * tec3d_surface_vert.vert
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 15/03/17.
 *
 */
/******************************************************************************/
attribute vec3	vertexPosition;
attribute vec2	texCoord;


uniform mat4	projectionMatrix;
uniform mat4	modelViewMatrix;
uniform mat4	normalMatrix;

varying vec2	TexCoord;

/******************************************************************************/
// main
/******************************************************************************/
void main()
{
	// Position
	vec4 pos		= projectionMatrix*modelViewMatrix*vec4(vertexPosition,1.0);
	gl_Position		= pos;
	TexCoord	= texCoord;
	
}
/******************************************************************************/
