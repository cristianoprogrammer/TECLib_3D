/******************************************************************************/
/*
 * TEC3DRender.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 03/02/17.
 *
 */
/******************************************************************************/
#include "TEC3DRender.hpp"

using namespace TEC3D;

/******************************************************************************/

/*.............................................................................*/
// Render
/*.............................................................................*/
Render::Render() {
	this->m_backgroundColor = {1.0f,1.0f,1.0f,1.0f};
	this->m_transparentBackground = GL_FALSE;
}

/*.............................................................................*/
// Set Perspective mode
/*.............................................................................*/
GLvoid Render::setPerspective() {
	
	this->m_projectionMatrix = glm::perspective(45.0f, 0.75f,0.1f, 10000.0f);
	//this->m_projectionMatrix = glm::scale(this->m_projectionMatrix, glm::vec3(1.0f,1.0f,1.0f));
	this->m_projection = glGetUniformLocation(this->m_shader->program, "projectionMatrix");

	
}

GLvoid Render::setInversePerspective() {
	
	this->m_projectionMatrix = glm::perspective(45.0f, 0.75f,1.0f, 10000.0f);
	this->m_projectionMatrix = glm::scale(this->m_projectionMatrix, glm::vec3(1.0f,-1.0f,1.0f));
	this->m_projection = glGetUniformLocation(this->m_shader->program, "projectionMatrix");
	
}

/*.............................................................................*/
// Set Orthographic mode
/*.............................................................................*/
GLvoid Render::setOrthographic() {
	
	this->m_projectionMatrix = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1000.0f);
	this->m_projection = glGetUniformLocation(this->m_shader->program, "projectionMatrix");
}

/*.............................................................................*/
// Set clear color with a Color4f
/*.............................................................................*/
GLvoid Render::setClearColor(Color4f color) {
	this->m_backgroundColor = color;
}

/*.............................................................................*/
// Set clear color with r,g,b,a float
/*.............................................................................*/
GLvoid Render::setClearColor(float r, float g, float b, float a) {
	this->m_backgroundColor = {r,g,b,a};
}

/*.............................................................................*/
// Clear OES scene
/*.............................................................................*/
GLvoid Render::clear() {
	
	
	glUniformMatrix4fv(this->m_projection, 1, GL_FALSE, glm::value_ptr(this->m_projectionMatrix));
	
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(this->m_backgroundColor.r,this->m_backgroundColor.g,
				 this->m_backgroundColor.b,this->m_backgroundColor.a);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	if (this->m_transparentBackground) {

//	}
	
	glClearDepthf(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	
}

/*.............................................................................*/
// Attach a shader program to render
/*.............................................................................*/
GLvoid Render::attachShader(Shader* shader) {
	
	this->m_shader = shader;
}

/*.............................................................................*/
// Get render shader program
/*.............................................................................*/
Shader* Render::getShader() {
	return this->m_shader;
}

/******************************************************************************/
