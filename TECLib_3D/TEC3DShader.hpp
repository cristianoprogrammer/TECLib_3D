/******************************************************************************/
/*
 * TEC3DShader.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 31/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DShader_hpp
#define TEC3DShader_hpp

#include "TEC3DBase.h"

/******************************************************************************/
// TEC3DShader
/******************************************************************************/
namespace TEC3D {
	
	/**************************************************************************/
	// Shader
	/**************************************************************************/
	class Shader {
		
	public:
		Shader();
		Shader(std::string vertex, std::string fragment);
	
	/*........................................................................*/
	// Public properties
	/*........................................................................*/
	public:
		GLuint program;
		GLint success;
		std::string fragmentCompileMessage;
		std::string vertexCompileMessage;
	
	/*........................................................................*/
	// Private method's
	/*........................................................................*/
	private:
		GLint compileSuccess;
		
		std::string fragmentShader;
		std::string vertexShader;
		
		GLuint fragmentId;
		GLuint vertexId;
		
	/*........................................................................*/
	// Public method's
	/*........................................................................*/
	public:
		GLint setFragment(std::string fragment);
		GLint setVertex(std::string vertex);
		GLvoid useProgram();
		
		GLint setupShaderProgram();
		GLuint getVertexShader();
		GLuint getFragmentShader();
		
	/*........................................................................*/
	// Private method's
	/*........................................................................*/
	private:
		GLint compileFragmentShader();
		GLint compileVertexShader();
	};

	
}

#endif /* TEC3DShader_hpp */
