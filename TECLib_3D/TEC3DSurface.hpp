/******************************************************************************/
/*
 * TEC3DSurface.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 15/03/17.
 *
 */
/******************************************************************************/
#ifndef TEC3DSurface_hpp
#define TEC3DSurface_hpp

#include "TEC3DBase.h"
#include "TEC3DTex2D.hpp"
#include "TEC3DShader.hpp"
#include "TEC3DRender.hpp"

/******************************************************************************/
// TEC3DSurface
/******************************************************************************/
namespace TEC3D {
	
	
	/**************************************************************************/
	// Surface
	/**************************************************************************/
	class Surface {
	
	/*........................................................................*/
	// Public properties
	/*........................................................................*/
	public:
	
		Vector2f position;
		Vector2f size;
		Tex2D texture;
		GLfloat angle;
		
	/*........................................................................*/
	// Private properties
	/*........................................................................*/
	private:
		glm::mat4 m_matrix;
		Vector2f m_size;
		
		GLuint m_vao;
		GLuint m_vbo[2];
		GLuint m_ebo;
		Shader* m_program;
		
		GLboolean m_firstFrame;
		
	/*........................................................................*/
	// Public method's
	/*........................................................................*/
	public:
		Surface();
		Surface(std::string filePath);
		
		GLvoid loadTexture(std::string filePath);
		GLvoid setTextureFromBytes(GLvoid* bytes, GLuint pixelType, GLsizei width,
								   GLsizei height);
		GLvoid render(Render* render);
		
		GLvoid setScale(GLfloat x, GLfloat y, GLfloat z);
		GLvoid setTranslate(GLfloat x, GLfloat y, GLfloat z);
	
	/*........................................................................*/
	// Private method's
	/*........................................................................*/
	private:
		GLvoid setupVertexData();
		GLvoid setupProgram();
		GLvoid setupTextureData();
	};
	
	
}


#endif /* TEC3DSurface_hpp */
