/******************************************************************************/
/*
 * TEC3DFoundation.h
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DFoundation_h
#define TEC3DFoundation_h

#include "TEC3DBase.h"
#include "TEC3DShader.hpp"


/******************************************************************************/
// TEC3DFoundation
/******************************************************************************/
namespace TEC3D {

	static GLboolean DEBUG_ENABLED = GL_FALSE;
	
	/*........................................................................*/
	// Check for error in OES
	/*........................................................................*/
	template <typename C>
	__inline GLuint getOESError(C* _class, const char* function) {
		GLuint _gl_error;
		if ((_gl_error = glGetError())!=GL_NO_ERROR && DEBUG_ENABLED) {
			printf("*******************************************************\n");
			printf("TEC3D<%s::%s>: GL_ERROR(0x%x)\n",typeid(_class).name(),function,_gl_error);
			printf("*******************************************************\n");
			return TEC3D_ERROR;
		}
		return TEC3D_NO_ERROR;
	}
	
	/*........................................................................*/
	// Check for error in OES
	/*........................................................................*/
	__inline GLuint getOESError(const char* function) {
		GLuint _gl_error;
		if ((_gl_error = glGetError())!=GL_NO_ERROR && DEBUG_ENABLED) {
			printf("*******************************************************\n");
			printf("TEC3D<%s>: GL_ERROR(0x%x)\n",function,_gl_error);
			printf("*******************************************************\n");
			return TEC3D_ERROR;
		}
		return TEC3D_NO_ERROR;
	}
	
	/*........................................................................*/
	// Get a GLSL Shader log info
	/*........................................................................*/
	__inline GLvoid getGLSLShaderLog(Shader* shader, GLuint type) {
		char _glsl_message[512];
		glGetShaderInfoLog((type==TEC3D_VERTEX)?
						   shader->getVertexShader():shader->getFragmentShader(),
						   512, NULL, _glsl_message);
		std::string _message = std::string(_glsl_message);
		if (DEBUG_ENABLED) {
			printf("*******************************************************\n");
			printf("TEC3D<GLSL_%s>: \n%s\n",
				   (type==TEC3D_VERTEX)?TEC3D_VERTEX_STRING:TEC3D_FRAGMENT_STRING,_message.c_str());
			printf("*******************************************************\n");
		}
	}
	
	/*........................................................................*/
	// Get a GLSL Shader message
	/*........................................................................*/
	__inline GLvoid getGLSLShaderLog(Shader* shader, GLuint type, std::string* message) {
		char _glsl_message[512];
		glGetShaderInfoLog((type==TEC3D_VERTEX)?
						   shader->getVertexShader():shader->getFragmentShader(),
						   512, NULL, _glsl_message);
		message = new std::string(_glsl_message);
	}

	/*........................................................................*/
	// Get a GLSL Shader Program log info
	/*........................................................................*/
	__inline GLvoid getGLSLProgramLog(Shader* shader) {
		char _glsl_message[512];
		glGetProgramInfoLog(shader->program,
						   512, NULL, _glsl_message);
		std::string _message = std::string(_glsl_message);
		if (DEBUG_ENABLED) {
			printf("*******************************************************\n");
			printf("TEC3D<GLSL_PROGRAM>: \n%s\n",_message.c_str());
			printf("*******************************************************\n");
		}
	}
	
	/*........................................................................*/
	// Vector2
	/*........................................................................*/
	// Vector with 2 T {x,y}
	template <typename T>
	union _Vector2 {
		
		struct { T x, y; };
		T vertice[2];
		
	};
//	using Vector2f = _Vector2<GLfloat>;
//	using Vector2i = _Vector2<GLint>;
	
	
	/*........................................................................*/
	// Vector3
	/*........................................................................*/
	// Vector with 3 T {x, y, z}
	template <typename T>
	union _Vector3 {
		
		struct { T x, y, z;};
		T vertice[3];

	};
//	using Vector3f = _Vector3<GLfloat>;
//	using Vector3i = _Vector3<GLint>;
	
	
	/*........................................................................*/
	// Vector4
	/*........................................................................*/
	// Vector with 4 T {x, y, z, z}
	template <typename T>
	union _Vector4 {
		
		struct { T x, y, z, w;};
		T vertice[4];
		
	};
	//using Vector4f = _Vector4<GLfloat>;
	//using Vector4i = _Vector4<GLint>;

	/*........................................................................*/
	// Quarternion
	/*........................................................................*/

	// Quarternion structure  {x, y, z, w}
	template <typename T>
	union _Quaternion {
		
		struct { T x, y, z, w; };
		T vertice[4];
		
	};
	using Quaternionf = _Quaternion<GLfloat>;
	using Quaternioni = _Quaternion<GLint>;
	
	/*........................................................................*/
	// Color4f
	/*........................................................................*/
	
	template <typename T>
	union _Color3 {
		struct {T r, g, b; };
		T value[3];
	};
	
	using Color3f	= _Color3<GLfloat>;
	using Color3ui	= _Color3<GLuint>;
	using Color3i	= _Color3<GLint>;
	
	// RGBA float
	template <typename T>
	union _Color4 {
		struct { T r, g, b, a; };
		T value[4];
	};
	using Color4f	= _Color4<GLfloat>;
	using Color4ui	= _Color4<GLuint>;
	using Color4i	= _Color4<GLint>;
		
	}


/******************************************************************************/


/******************************************************************************/

#endif /* TECVertex_h */
