/******************************************************************************/
/*
 * TEC3DMesh.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#include "TEC3DMesh.hpp"


/******************************************************************************/
// TEC3DMesh
/******************************************************************************/
using namespace TEC3D;

/*.............................................................................*/
// Constructor
/*.............................................................................*/
Mesh::Mesh(Shader *shader, std::vector<Vertex> vertex, std::vector<GLuint> index,
		   std::vector<Tex2D> texture, Material material) {

	this->shader = shader;
	this->texture = texture;
	this->vertex = vertex;
	this->index	= index;
	this->material = material;
	
	this->prepare();
	
}

/*.............................................................................*/
// Render
/*.............................................................................*/
void Mesh::render(glm::mat4 matrix) {
	
	// Set textures
	for (GLuint i=0; i < this->texture.size(); i++ ) {
		
		glActiveTexture(GL_TEXTURE0 + i);
		if (!this->texture[i].type.compare("texture_ambient")) {
			glUniform1i(glGetUniformLocation(shader->program, "ambient_texture"), i);
		} else if (!this->texture[i].type.compare("texture_diffuse")) {
			glUniform1i(glGetUniformLocation(shader->program, "diffuse_texture"), i);
		} else if (!this->texture[i].type.compare("texture_specular")) {
			glUniform1i(glGetUniformLocation(shader->program, "specular_texture"), i);
		} else if (!this->texture[i].type.compare("texture_normals")) {
			glUniform1i(glGetUniformLocation(shader->program, "normal_texture"), i);
		}
		glBindTexture(GL_TEXTURE_2D, this->texture[i].id);
		
	}
	
	// Setting model matrix
	glm::mat4 transposeMatrix = glm::transpose(matrix);
	GLint modelMatrix = glGetUniformLocation(shader->program, "modelViewMatrix");
	glUniformMatrix4fv(modelMatrix, 1, GL_FALSE, glm::value_ptr(matrix));
	
	
	GLint normMatrix = glGetUniformLocation(shader->program, "normalMatrix");
	glUniformMatrix4fv(normMatrix, 1, GL_FALSE, glm::value_ptr(transposeMatrix));
	
	// Setting ambient color
	GLint ambientColorL = glGetUniformLocation(shader->program, "ambientColor");
	glUniform3f(ambientColorL, this->material.ambientColor.r, this->material.ambientColor.g,
				this->material.ambientColor.b);
	
	// Setting diffuse color
	GLint diffuseColorL = glGetUniformLocation(shader->program, "diffuseColor");
	glUniform3f(diffuseColorL, this->material.diffuseColor.r, this->material.diffuseColor.g, this->material.diffuseColor.b);
	
	// Setting specular color
	GLint specularColorL = glGetUniformLocation(shader->program, "specularColor");
	glUniform3f(specularColorL, this->material.specularColor.r, this->material.specularColor.g, this->material.specularColor.b);
	
	// Setting opacity
	GLint opacityL = glGetUniformLocation(shader->program, "opacity");
	glUniform1f(opacityL, this->material.opacity);
	
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(this->vao);
	//glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	
	// Normal or wireframe rendering
	if (this->material.wireframe) {
		// Wireframe rendering
		glDrawElements(GL_LINES, (GLsizei)this->index.size(), GL_UNSIGNED_INT, 0);
	} else {
		//
		glDrawElements(GL_TRIANGLES, (GLsizei)this->index.size(), GL_UNSIGNED_INT, 0);

	}
	
	glBindVertexArray(0);
	
	for (GLuint i=0; i < this->texture.size(); i++ ) {
		
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	if (!(getOESError(this,TEC3D_FUNCTION)==TEC3D_NO_ERROR)) {
		getGLSLProgramLog(shader);
		getGLSLShaderLog(shader, TEC3D_VERTEX);
	}
	
}

/*.............................................................................*/
// Update
/*.............................................................................*/
void Mesh::update(vector<glm::mat4x4> transforms) {

	std::vector<Vertex> vertexArray;
	vertexArray.reserve(this->vertex.size());
	
	for (int i=0; i<this->vertex.size();i++) {

		Vertex _vertex = this->vertex[i];
		glm::mat4x4 boneTransform = transforms[(GLint)_vertex.boneId.x] * _vertex.boneWeights.x;
		boneTransform += transforms[(GLint)_vertex.boneId.y] * _vertex.boneWeights.y;
		boneTransform += transforms[(GLint)_vertex.boneId.z] * _vertex.boneWeights.z;
		boneTransform += transforms[(GLint)_vertex.boneId.w] * _vertex.boneWeights.w;
		
		glm::vec4 _vertexPos = glm::vec4(_vertex.pos.x,_vertex.pos.y,_vertex.pos.z,0.0f);
		_vertexPos = boneTransform * _vertexPos;
		_vertex.pos.x = _vertexPos.x;
		_vertex.pos.y = _vertexPos.y;
		_vertex.pos.z = _vertexPos.z;
		vertexArray.push_back(_vertex);
		
	}
	
	
	glBindVertexArray(this->vao);
	
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

	glBufferSubData(GL_ARRAY_BUFFER, 0, vertexArray.size() * sizeof(Vertex), &vertexArray[0]);
	
	glBindVertexArray(0);
	
	getOESError(this,TEC3D_FUNCTION);
	
}


/*.............................................................................*/
// Prepare
/*.............................................................................*/
void Mesh::prepare() {
	
	glGenVertexArrays(1, &this->vao);
	glGenBuffers(1, &this->vbo);
	glGenBuffers(1, &this->ebo);
	//glGenBuffers(1, &this->bvb);
	
	glBindVertexArray(this->vao);
	
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferData(GL_ARRAY_BUFFER, this->vertex.size() * sizeof(Vertex), &this->vertex[0], GL_STATIC_DRAW);
	
	
	glEnableVertexAttribArray(TEC3DVertexAttribPosition);
	glVertexAttribPointer(TEC3DVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	
	glEnableVertexAttribArray(TEC3DVertexAttribNormal);
	glVertexAttribPointer(TEC3DVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, norm));
	
	glEnableVertexAttribArray(TEC3DVertexAttribTexCoord);
	glVertexAttribPointer(TEC3DVertexAttribTexCoord, 2, GL_FLOAT, GL_TRUE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texC));
	
	glEnableVertexAttribArray(TEC3DVertexAttribBoneId);
	glVertexAttribPointer(TEC3DVertexAttribBoneId, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, boneId));
	
	glEnableVertexAttribArray(TEC3DVertexAttribBoneWeight);
	glVertexAttribPointer(TEC3DVertexAttribBoneWeight, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, boneWeights));
	
	
	/*glBindBuffer(GL_ARRAY_BUFFER, this->bvb);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexBoneData) * this->bones.size(), &this->bones[0], GL_STATIC_DRAW);*/
	
	/*glEnableVertexAttribArray(TEC3DVertexAttribBoneId);
	glVertexAttribPointer(TEC3DVertexAttribBoneId, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)0);
	
	printf("OFFSET: %lu",offsetof(VertexBoneData, Weights));
	
	glEnableVertexAttribArray(TEC3DVertexAttribBoneWeight);
	glVertexAttribPointer(TEC3DVertexAttribBoneWeight, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (GLvoid*)offsetof(VertexBoneData, Weights));*/
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->index.size() * sizeof(GLuint), this->index.data(), GL_STATIC_DRAW);
	
	/*glDisableVertexAttribArray(TEC3DVertexAttribPosition);
	glDisableVertexAttribArray(TEC3DVertexAttribNormal);
	glDisableVertexAttribArray(TEC3DVertexAttribTexCoord);
	glDisableVertexAttribArray(TEC3DVertexAttribBoneId);
	glDisableVertexAttribArray(TEC3DVertexAttribBoneWeight);*/
	
	glBindVertexArray(0);
	
	this->vertex.clear();
	this->vertex = std::vector<Vertex>();
	
	getOESError(this,TEC3D_FUNCTION);
	
}

/******************************************************************************/
