/******************************************************************************/
/*
 * TEC3DModel.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DModel_hpp
#define TEC3DModel_hpp

#include "TEC3DBase.h"

#include "TEC3DMesh.hpp"
#include "TEC3DShader.hpp"
#include "TEC3DMatrix.hpp"

#include "assimp/mesh.h"
#include "assimp/scene.h"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"

#ifdef __APPLE__
#include "TEC3DTextureLoader.h"
#endif

#ifdef __cplusplus

#include <cstdlib>

#endif

/******************************************************************************/
// TEC3DModel
/******************************************************************************/
namespace TEC3D {
	
	
	/**************************************************************************/
	// Model
	/**************************************************************************/
	class Model {
	
	/*........................................................................*/
	// Public properties
	/*........................................................................*/
	public:
		
		std::vector<Mesh>			mesh;
		glm::mat4					matrix;
		
		
	/*........................................................................*/
	// Private properties
	/*........................................................................*/
	private:
		
		std::string					file_dir;
		std::string					file_path;
		std::vector<Tex2D>			textures;
		std::map<string,uint>		boneMapping;
		uint						numBones;
		std::vector<BoneInfo>		boneInfo;
		Matrix4f					globalInverseTransform;
		const aiScene *				m_scene;
		std::vector<uint>			boneLocation;
		GLfloat						m_startTime;
		Assimp::Importer			importer;
		GLuint						bvb;
		GLuint						numVertices;
		GLboolean					boneLocations;
		std::vector<uint>			entries;
		GLuint						meshIndex;
		Shader*						shader;
		GLboolean					playing;
		GLuint						boneCounter;
		
	/*........................................................................*/
	// Public method's
	/*........................................................................*/
	public:
		
		Model(std::string dir, std::string name, Shader* shader);
		Model(const GLvoid* buffer, size_t length);
		~Model();
		
		GLvoid play(double time);
		GLvoid render(double time);
	
	/*........................................................................*/
	// Private method's
	/*........................................................................*/
	private:
		
		GLvoid load(const GLvoid* buffer, size_t length);
		GLvoid load(const std::string name);
		
		GLvoid processNode(aiNode* node, const aiScene* scene);
		Mesh processMesh(aiMesh* mesh, const aiScene* scene);
		GLvoid processMeshBones(aiMesh* mesh);
		
		std::vector<Tex2D> processMaterial(aiMaterial* material);
		std::vector<Tex2D> processMaterialTextures(aiMaterial* mat, aiTextureType type,
												std::string typeName);
		
		GLvoid initBoneLocations(Shader* shader);
		GLvoid loadBones(aiMesh* mesh, std::vector<Vertex>* vertices);
		GLvoid boneTransform(float TimeInSeconds, vector<Matrix4f>& Transforms);
		GLvoid readNodeHeirarchy(float AnimationTime, const aiNode* pNode, const Matrix4f& ParentTransform);
		const aiNodeAnim* findNodeAnim(const aiAnimation* pAnimation, const string NodeName);
		const aiMeshAnim* findMeshAnim(const aiAnimation* pAnimation, const string NodeName);
		
		GLvoid calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
		GLvoid calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
		GLvoid calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
		uint findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
		uint findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
		uint findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
		glm::mat4x4 glmMat4x4(Matrix4f _matrix);
		Vector3f vector3fFrom(aiVector3D* vector3D);
		Vector2f vector2fFrom(aiVector3D* vector2D);
		Material getMaterialProperties(const aiMaterial* pMaterial);
	};
	
	
}


#endif /* TEC3DModel_hpp */
