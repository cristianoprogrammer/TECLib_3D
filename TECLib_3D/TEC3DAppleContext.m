/******************************************************************************/
/*
 * TEC3DFoundation.m
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 16/03/17.
 *
 */
/******************************************************************************/

#import "TEC3DAPPLEContext.h"

/******************************************************************************/
@implementation TEC3DAppleContext


+ (EAGLContext*) sharedContext {
	static EAGLContext* _context;
	static dispatch_once_t _once;
	
	dispatch_once(&_once, ^{
		_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
	});
	return _context;
}

+ (EAGLContext*) sharedContextGPUSharedGroup {
	static EAGLContext* _context;
	static dispatch_once_t _once;
	
	dispatch_once(&_once, ^{
		//_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:[[[GPUImageContext sharedImageProcessingContext] context] sharegroup]];
		_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
	});
	[EAGLContext setCurrentContext:_context];
	return _context;
}


@end
/******************************************************************************/
