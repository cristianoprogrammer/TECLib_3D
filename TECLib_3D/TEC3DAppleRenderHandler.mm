/******************************************************************************/
/*
 * TEC3DAppleRenderHandler.m
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 16/03/17.
 *
 */
/******************************************************************************/
#import "TEC3DAppleRenderHandler.h"

/******************************************************************************/
@interface TEC3DAppleRenderHandler()


@end

/******************************************************************************/
@implementation TEC3DAppleRenderHandler

/*.............................................................................*/
// Init with a GLKView
/*.............................................................................*/

- (id) initWithGLKView:(GLKView*)glkView {
	
	self = [super init];
	if (self != nil) {
		self->m_glkView = glkView;
		self->m_defaultRenderEnabled = YES;
		[TEC3DAppleContext sharedContextGPUSharedGroup];
		[self setupDefaultRender];
	}
	
	return self;
}

- (id)	initWithGLKView:(GLKView*)glkView render:(TEC3D::Render*) render {
	self = [super init];
	if (self != nil) {
		self->m_glkView = glkView;
		
		self->m_defaultRenderEnabled = YES;
		[TEC3DAppleContext sharedContextGPUSharedGroup];
		[self setupDefaultRender];
	}
	
	return self;
}

/*.............................................................................*/
// Setup OpenGL ES
/*.............................................................................*/
- (void) setupOES {
	
	self->m_glkView.enableSetNeedsDisplay = YES;
	self->m_glkView.delegate = self;
	self->m_glkView.context = [TEC3DAppleContext sharedContextGPUSharedGroup];
	self->m_glkView.drawableDepthFormat = GLKViewDrawableDepthFormat16;
	[self->m_glkView setHidden:NO];
	self->m_eaglLayer = (CAEAGLLayer*) self->m_glkView.layer;
	self->m_eaglLayer.opaque = NO;
	self->m_eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
}

/*.............................................................................*/
// Setup default Render
/*.............................................................................*/
- (void) setupDefaultRender {
	
	self->m_render = new TEC3D::Render();
	
	//[self setupDefaultSurfaceShader];
	
	//self->m_render->attachShader(self->m_defaultShader);
	//self->m_render->setClearColor(0.0f,0.0f,0.0f,1.0f);
	
}

/*.............................................................................*/
// Setup default Surface Shader
/*.............................................................................*/
- (void) setupDefaultSurfaceShader {
	
	
	NSString* fragPath = [[NSBundle mainBundle] pathForResource:@"tec3d_surface_frag" ofType:@"frag"];
	NSString* vertPath = [[NSBundle mainBundle] pathForResource:@"tec3d_surface_vert" ofType:@"vert"];
	
	NSData* fragData = [[NSData alloc] initWithContentsOfFile:fragPath];
	NSData* vertData = [[NSData alloc] initWithContentsOfFile:vertPath];
	
	NSString* fragString	= [[NSString alloc] initWithData:fragData encoding:NSUTF8StringEncoding];
	NSString* vertString	= [[NSString alloc] initWithData:vertData encoding:NSUTF8StringEncoding];
	
	const char* fragmentC	= [fragString cStringUsingEncoding:NSUTF8StringEncoding];
	const char* vertexC		= [vertString cStringUsingEncoding:NSUTF8StringEncoding];
	
	std::string fragmentShader(fragmentC);
	std::string vertexShader(vertexC);
	
	self->m_defaultShader = new TEC3D::Shader(vertexShader,fragmentShader);
	
}

/*.............................................................................*/
// Set delegate to render
/*.............................................................................*/
- (void) setDelegate:(id<TEC3DAppleRenderDelegate>)delegate {
	
	if (delegate != nil) {
		self->m_delegate = delegate;
		[self setupOES];
	}
}

/*.............................................................................*/
// Set to RenderTarget delegate
/*.............................................................................*/
- (void) setRenderTargetDelegate:(id<TEC3DAppleRenderTargetDelegate>)delegate {
	
	if (delegate != nil) {
		self->m_renderTargetDelegate = delegate;
	}
}

/*.............................................................................*/
// Set ImageView
/*.............................................................................*/
- (void) setImageView:(UIImageView*) imageView {
	self->m_imageView = imageView;
}

/*.............................................................................*/
// Prepare to RenderTarget mode
/*.............................................................................*/
- (void) prepareToRenderTarget:(CGSize)resultSize {
	
	self->m_fbSize = resultSize;
	
	[TEC3DAppleContext sharedContextGPUSharedGroup];
	
	glGenFramebuffers(1, &self->m_frameBufferID);
	glBindFramebuffer(GL_FRAMEBUFFER, self->m_frameBufferID);
	
	/*glGenRenderbuffers(1, &self->m_renderBufferID);
	glBindRenderbuffer(GL_RENDERBUFFER, self->m_renderBufferID);
	
	if (![[TEC3DAppleContext sharedContextGPUSharedGroup] renderbufferStorage:GL_RENDERBUFFER fromDrawable:self->m_eaglLayer]) {
		NSLog(@"TEC3D RenderBufferStorage error!");

	}
	
	GLint backingWidth, backingHeight;
	
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
	
	NSLog(@"TEC3D RenderBuffer width: %d, %d!",backingWidth,backingHeight);
	if ( (backingWidth == 0) || (backingHeight == 0) )
	{
		
		return;
	}
	
	self->m_fbSize = CGSizeMake((float)backingWidth, (float)backingHeight);
	
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, self->m_fbSize.width,
						  self->m_fbSize.height);
	
	glGenRenderbuffers(1, &self->m_depthRenderBufferID);
	glBindRenderbuffer(GL_RENDERBUFFER, self->m_depthRenderBufferID);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, self->m_fbSize.width, self->m_fbSize.height);
	
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, self->m_renderBufferID);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, self->m_depthRenderBufferID);
	
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) ;
	if(status != GL_FRAMEBUFFER_COMPLETE) {
		NSLog(@"TEC3D failed to make complete framebuffer object %x", status);
	}*/
	
	CFDictionaryRef empty;
	CFMutableDictionaryRef attrs;
	
	empty = CFDictionaryCreate(kCFAllocatorDefault,
							   NULL,
							   NULL,
							   0,
							   &kCFTypeDictionaryKeyCallBacks,
							   &kCFTypeDictionaryValueCallBacks);
	
	attrs = CFDictionaryCreateMutable(kCFAllocatorDefault,
									  1,
									  &kCFTypeDictionaryKeyCallBacks,
									  &kCFTypeDictionaryValueCallBacks);
	
	CFDictionarySetValue(attrs,
						 kCVPixelBufferIOSurfacePropertiesKey,
						 empty);
	
	CVReturn error = CVPixelBufferCreate ( kCFAllocatorDefault, self->m_fbSize.width , self->m_fbSize.height, kCVPixelFormatType_32BGRA,
										  attrs,
										  &self->m_fbPixelBuffer ) ;
	
	if(error != kCVReturnSuccess || self->m_fbPixelBuffer == NULL) {
		NSLog(@"TEC3D CVPixelBufferRef create error: %d",error);
		return ;
	}
	
	CVOpenGLESTextureCacheRef coreVideoTextureCache = NULL;
	CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, [TEC3DAppleContext sharedContextGPUSharedGroup], NULL, &coreVideoTextureCache);

	
	error = CVOpenGLESTextureCacheCreateTextureFromImage (
														  kCFAllocatorDefault,
														  coreVideoTextureCache,
														  self->m_fbPixelBuffer,
														  NULL,
														  GL_TEXTURE_2D,
														  GL_RGBA,
														  self->m_fbSize.width,
														  self->m_fbSize.height,
														  GL_BGRA,
														  GL_UNSIGNED_BYTE,
														  0,
														  &self->m_fbTextureRef);
	
	if (error != kCVReturnSuccess) {
		NSLog(@"TEC3D Error on create texture from image: %d",error);
		return ;
	}
	
	self->m_fbTextureID = CVOpenGLESTextureGetName(self->m_fbTextureRef);
	self->m_fbTarget = CVOpenGLESTextureGetTarget(self->m_fbTextureRef);
	glBindTexture(GL_TEXTURE_2D, self->m_fbTextureID);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	glBindFramebuffer(GL_FRAMEBUFFER, self->m_frameBufferID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
						   GL_TEXTURE_2D, self->m_fbTextureID, 0);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	TEC3D::getOESError(self,TEC3D_FUNCTION);
	
	CFRelease(attrs);
	CFRelease(empty);
	CFRelease(coreVideoTextureCache);
	
	
	//self->m_multicameraEnabled = YES;
	self->m_renderSurface = new TEC3D::Surface();
	self->m_renderSurface->setScale(0.45f, 0.57f, 0.1f);
	self->m_renderSurface->setTranslate(0.05f, 0.0f, -0.10f);
	
	self->m_renderTargetEnabled = YES;
	//[self setupDefaultSurfaceShader];
	
}

/*.............................................................................*/
// Prepare to multicamera system
/*.............................................................................*/
- (void) prepareToMulticamera {
	[TEC3DAppleContext sharedContextGPUSharedGroup];
	
	self->m_multicameraEnabled = YES;
	self->m_mainCamSurface = new TEC3D::Surface();
	self->m_secondCamSurface = new TEC3D::Surface();
	self->m_mainCamSurface->setScale(0.4275f, 0.57f, 0.1f);//0.42f, 0.56f, 0.1f
	self->m_mainCamSurface->setTranslate(0.0f, 0.0f, -0.11f);
	self->m_secondCamSurface->setScale(0.12f, 0.12f, 0.1f);
	self->m_secondCamSurface->setTranslate(-2.0f, -2.4f, -0.1f);
}

/*.............................................................................*/
// Set maim camera image from a bytes
/*.............................................................................*/
- (void) setMainCamImageFromBytes:(GLvoid*) data width:(NSInteger)width height:(NSInteger)height {

	self->m_mainCamSurface->setTextureFromBytes(data, GL_BGRA, width, height);
}

/*.............................................................................*/
// Set second camera image from bytes
/*.............................................................................*/
- (void) setSecondCamImageFromBytes:(GLvoid*) data width:(NSInteger)width height:(NSInteger)height {
	
	self->m_secondCamSurface->setTextureFromBytes(data, GL_BGRA,  width, height);
}

/*.............................................................................*/
// Start render handler
/*.............................................................................*/
- (void) start {
	
	
	self->m_displayLink = [CADisplayLink displayLinkWithTarget:self
													  selector:@selector(prepareForRender)];
	
	[self->m_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[self->m_displayLink setPreferredFramesPerSecond:30];
	
	
	
//	[self->m_displayLink ];
}

/*.............................................................................*/
// Prepare to render
/*.............................................................................*/
- (void) prepareForRender {
	//[TEC3DAppleContext sharedContextGPUSharedGroup];
	
	// Bind FrameBuffer to OES Context
	/*if (self->m_renderTargetEnabled) {
		glBindFramebuffer(GL_FRAMEBUFFER, self->m_frameBufferID);
		glViewport(0, 0,480, 640);
	}
	
	// Use default render
	if (self->m_defaultRenderEnabled) {
		
		self->m_defaultShader->useProgram();
		//self->m_render->setInversePerspective();
		self->m_render->setClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		self->m_render->clear();
		
	}
	
	// Multicamera
	if (self->m_multicameraEnabled) {
		
		//self->m_mainCamSurface->render(self->m_render);
		//self->m_secondCamSurface->render(self->m_render);
		
	}
	
	if (self->m_delegate != nil && [self->m_delegate respondsToSelector:@selector(tec3d_render)]) {
		
		[self->m_delegate tec3d_render];
	}
	
	// RenderTarget result
	CVPixelBufferLockBaseAddress(self->m_fbPixelBuffer, kCVPixelBufferLock_ReadOnly);
	//[self drawToImageView];
	if (self->m_renderTargetEnabled && self->m_renderTargetDelegate != nil && [self->m_renderTargetDelegate respondsToSelector:@selector(tec3d_render_result:)]) {
		
		[self->m_renderTargetDelegate tec3d_render_result:self->m_fbPixelBuffer];
		
	}
	
	CVPixelBufferUnlockBaseAddress(self->m_fbPixelBuffer, kCVPixelBufferLock_ReadOnly);
	
	// Back to default FrameBuffer
	if (self->m_renderTargetEnabled) {
		/*glBindRenderbuffer(GL_RENDERBUFFER, self->m_renderBufferID);
		 [[TEC3DAppleContext shared] presentRenderbuffer:GL_RENDERBUFFER];*/
	//}

	//glBindFramebuffer(GL_FRAMEBUFFER,0);

	if (self->m_glkView != nil) {
		[self->m_glkView display];
	}
	
}

/*.............................................................................*/
// Draw pixelbuffer to ImageView
/*.............................................................................*/
- (void) drawToImageView {
	
	uint8_t* baseAddress = (uint8_t*) CVPixelBufferGetBaseAddress(self->m_fbPixelBuffer);
	int width = (int)CVPixelBufferGetWidth(self->m_fbPixelBuffer);
	int height = (int)CVPixelBufferGetHeight(self->m_fbPixelBuffer);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = CGBitmapContextCreate(
							baseAddress, width, height, 8, CVPixelBufferGetBytesPerRow(m_fbPixelBuffer),
							colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst );
	CGImageRef quartzImage = CGBitmapContextCreateImage(context);
	
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
	
	UIImage *image = [UIImage imageWithCGImage:quartzImage];
	CGImageRelease(quartzImage);

	if (self->m_imageView!=nil)
		[self->m_imageView performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];
}

/*.............................................................................*/
// GLKViewDelegate
/*.............................................................................*/
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {

	/*GLint defaultFBO;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &defaultFBO);
	
	glBindFramebuffer(GL_FRAMEBUFFER, defaultFBO);*/

	//glViewport(0, 0,  [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
	//glViewport(0, 0, self->m_glkView.frame.size.width, self->m_glkView.frame.size.height);
	if (self->m_defaultRenderEnabled) {
		
		//self->m_defaultShader->useProgram();
		
		//self->m_render->setOrthographic();
		//self->m_render->setClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		//self->m_render->clear();
		
	}

	
	/*CVPixelBufferLockBaseAddress(self->m_fbPixelBuffer, kCVPixelBufferLock_ReadOnly);
	size_t width = CVPixelBufferGetWidth(self->m_fbPixelBuffer);
	size_t height = CVPixelBufferGetHeight(self->m_fbPixelBuffer);
	self->m_renderSurface->setTextureFromBytes(CVPixelBufferGetBaseAddress(self->m_fbPixelBuffer), GL_BGRA,(GLsizei) width, (GLsizei)height);
	CVPixelBufferUnlockBaseAddress(self->m_fbPixelBuffer, kCVPixelBufferLock_ReadOnly);
	
	self->m_renderSurface->render(self->m_render);*/
	[self->m_delegate tec3d_render];
	
}


@end
/******************************************************************************/
