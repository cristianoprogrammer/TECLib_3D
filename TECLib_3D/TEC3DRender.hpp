/******************************************************************************/
/*
 * TEC3DRender.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 03/02/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DRender_hpp
#define TEC3DRender_hpp

#include "TEC3DBase.h"
#include "TEC3DShader.hpp"
#include "TEC3DFoundation.h"

/******************************************************************************/
// TEC3DRender
/******************************************************************************/
namespace TEC3D {
	
	/**************************************************************************/
	// Render
	/**************************************************************************/
	class Render {

	/*........................................................................*/
	// Private properties
	/*........................................................................*/
	private:
		glm::mat4 m_projectionMatrix;
		glm::mat4 m_viewMatrix;
		
		Shader*		m_shader;
		GLint		m_projection;
		GLboolean	m_transparentBackground;
		Color4f		m_backgroundColor;
	
	/*........................................................................*/
	// Public method's
	/*........................................................................*/
	public:
		Render();
		GLvoid setPerspective();
		GLvoid setInversePerspective();
		GLvoid setOrthographic();
		
		GLvoid setClearColor(Color4f color);
		GLvoid setClearColor(float r, float g, float b, float a);
		
		GLvoid clear();
		GLvoid attachShader(Shader* shader);
		Shader* getShader();
		
	};
	
}
/******************************************************************************/

#endif /* TEC3DRender_hpp */
