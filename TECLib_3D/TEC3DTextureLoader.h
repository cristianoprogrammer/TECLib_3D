/******************************************************************************/
/*
 * TEC3DTextureLoader.h
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 31/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DTextureLoader_h
#define TEC3DTextureLoader_h

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <GLKit/GLKit.h>
#endif

#include "TEC3DBase.h"
#include "TEC3DTex2D.hpp"
#include "TEC3DFoundation.h"

/******************************************************************************/
// TEC3DTextureLoader
/******************************************************************************/
namespace TEC3D {
	
	/**************************************************************************/
	// TextureLoader
	/**************************************************************************/
	class TextureLoader {
		
	/*........................................................................*/
	// Public properties
	/*........................................................................*/
	public:
		
		static GLuint LoadTexture(std::string imagefile);
		
		static GLuint LoadTexture(Tex2D* texture);
		
	};

	
};



#endif /* TEC3DTextureLoader_h */
