/******************************************************************************/
/*
 * TEC3DSurface.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 15/03/17.
 *
 */
/******************************************************************************/

#include "TEC3DSurface.hpp"

using namespace TEC3D;

/******************************************************************************/

/*.............................................................................*/
// Constructor
/*.............................................................................*/
Surface::Surface() {
	
	this->setupVertexData();
	this->setupTextureData();
	
}

/*.............................................................................*/
// Constructor with a path to texture
/*.............................................................................*/
Surface::Surface(std::string filePath) {
	
}

/*.............................................................................*/
// Setupe vertex data
/*.............................................................................*/
GLvoid Surface::setupVertexData() {
	
	GLfloat vertices[] = {
		1.0f,  1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f
	};
	
	GLuint indices[] = {
		0, 1, 3,
		1, 2, 3
	};
	
	GLfloat textureCoords[] = {
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f
	};
	
	glGenVertexArrays(1, &this->m_vao);
	glGenBuffers(2, &this->m_vbo[0]);
	glGenBuffers(1, &this->m_ebo);
	
	glBindVertexArray(this->m_vao);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, this->m_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (GLvoid*)0);
	
	glBindBuffer(GL_ARRAY_BUFFER, this->m_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords), textureCoords, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), (GLvoid*)0);
	
	glBindVertexArray(0);
	
	this->m_matrix = glm::mat4();
	this->setScale(1.0f, 1.0f, 1.0f);
	this->setTranslate(0.0f, 0.0f, -1.0f);
}

/*.............................................................................*/
// Setup texture data
/*.............................................................................*/
GLvoid Surface::setupTextureData() {
	
	this->texture.type = "ambient_texture";
	glGenTextures(1, &this->texture.id);
	this->m_firstFrame = GL_TRUE;
	
}

GLvoid Surface::setupProgram() {
	
}


/*.............................................................................*/
// Load texture from file
/*.............................................................................*/
GLvoid Surface::loadTexture(std::string filePath) {
	
}

/*.............................................................................*/
// Set the texture from a byte array
/*.............................................................................*/
GLvoid Surface::setTextureFromBytes(GLvoid* bytes, GLuint pixelType, GLsizei width,
									GLsizei height) {
	
	
	if (this->m_firstFrame) {
		
		glBindTexture(GL_TEXTURE_2D,this->texture.id);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		//if (!isPowOf2) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		//}
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, pixelType, GL_UNSIGNED_BYTE, bytes);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		
		this->m_firstFrame = GL_FALSE;
		
	} else {
		
		glBindTexture(GL_TEXTURE_2D,this->texture.id);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		//if (!isPowOf2) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		//}
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, pixelType, GL_UNSIGNED_BYTE, bytes);
		
		
		
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);
	
}

/*.............................................................................*/
// Render
/*.............................................................................*/
GLvoid Surface::render(Render* render) {
	
	
	GLint modelMatrix = glGetUniformLocation(render->getShader()->program, "modelViewMatrix");
	glUniformMatrix4fv(modelMatrix, 1, GL_FALSE, glm::value_ptr(this->m_matrix));
	GLint diffuseColorL = glGetUniformLocation(render->getShader()->program, "ambientColor");
	glUniform3f(diffuseColorL, 0.5f, 0.5f, 0.5f);
	glActiveTexture(GL_TEXTURE0);
	glUniform1f(glGetUniformLocation(render->getShader()->program, "ambient_texture"), 0);
	glBindTexture(GL_TEXTURE_2D,
				  this->texture.id);
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(0);
	
	
}

/*.............................................................................*/
// Set Scale
/*.............................................................................*/
GLvoid Surface::setScale(GLfloat x, GLfloat y, GLfloat z) {
	this->m_matrix = glm::scale(this->m_matrix, glm::vec3(x,y,z));
}

/*.............................................................................*/
// Set Translate
/*.............................................................................*/
GLvoid Surface::setTranslate(GLfloat x, GLfloat y, GLfloat z) {
	this->m_matrix = glm::translate(this->m_matrix, glm::vec3(x,y,z));
	
}

/******************************************************************************/
