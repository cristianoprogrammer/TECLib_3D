/******************************************************************************/
/*
 * TEC3DAppleContext.h
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 16/03/17.
 *
 */
/******************************************************************************/

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "GPUImage.h"

/******************************************************************************/
@interface TEC3DAppleContext : NSObject
{
	
}

+ (EAGLContext*) sharedContext;

+ (EAGLContext*) sharedContextGPUSharedGroup;

@end
/******************************************************************************/
