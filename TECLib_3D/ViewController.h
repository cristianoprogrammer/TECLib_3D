//
//  ViewController.h
//  TECLib_3D
//
//  Created by Cristiano Douglas on 25/01/17.
//  Copyright © 2017 Tecvidya Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

#import "TEC3DAppleRenderHandler.h"

@interface ViewController : UIViewController <TEC3DAppleRenderDelegate, TEC3DAppleRenderTargetDelegate>


@end

