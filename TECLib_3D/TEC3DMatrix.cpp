/******************************************************************************/
/*
 * TEC3DMatrix.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 2/02/17.
 *
 */
/******************************************************************************/

#include "TEC3DMatrix.hpp"

using namespace TEC3D;

/*.............................................................................*/
// Constructor's
/*.............................................................................*/
Matrix4x4::Matrix4x4(GLfloat a0, GLfloat a1, GLfloat a2, GLfloat a3,
		  GLfloat b0, GLfloat b1, GLfloat b2, GLfloat b3,
		  GLfloat c0, GLfloat c1, GLfloat c2, GLfloat c3,
		  GLfloat d0, GLfloat d1, GLfloat d2, GLfloat d3) {
	
	this->m[0][0] = a0; this->m[0][1] = a1; this->m[0][2] = a2; this->m[0][3] = a3;
	this->m[1][0] = b0; this->m[1][1] = b1; this->m[1][2] = b2; this->m[1][3] = b3;
	this->m[2][0] = c0; this->m[2][1] = c1; this->m[2][2] = c2; this->m[2][3] = c3;
	this->m[3][0] = d0; this->m[3][1] = d1; this->m[3][2] = d2; this->m[3][3] = d3;
}

Matrix4x4::Matrix4x4(GLfloat matrix[4][4]) {
	for (int x=0; x<4; x++) {
		for (int y=0; y<4; y++) {
			this->m[x][y] = matrix[x][y];
		}
	}
}

Matrix4x4::Matrix4x4() {
	
}

/*.............................................................................*/
// Get matrix identity
/*.............................................................................*/
GLvoid Matrix4x4::identity() {
	for (int x=0; x<4; x++) {
		for (int y=0; y<4; y++) {
			this->m[x][y] = this->identityMatrix[x][y];
		}
	}
}

/*.............................................................................*/
// Rotate with x, y, z
/*.............................................................................*/
GLvoid Matrix4x4::rotate(GLfloat x, GLfloat y, GLfloat z) {

	this->rotateX(x);
	this->rotateY(y);
	this->rotateZ(z);
}

/*.............................................................................*/
// Rotate with Vector3f
/*.............................................................................*/
GLvoid Matrix4x4::rotate(Vector3f vector) {
	
	this->rotateX(vector.x);
	this->rotateY(vector.y);
	this->rotateZ(vector.z);
	
}

/*.............................................................................*/
// Rotate x
/*.............................................................................*/
GLvoid Matrix4x4::rotateX(GLfloat theta) {
	
	this->m[1][1] =  cosf(tec3d_radians(theta));
	this->m[1][2] = -sinf(tec3d_radians(theta));
	this->m[2][1] =  sinf(tec3d_radians(theta));
	this->m[2][2] =  cosf(tec3d_radians(theta));
}

/*.............................................................................*/
// Rotate y
/*.............................................................................*/
GLvoid Matrix4x4::rotateY(GLfloat theta) {
	
	this->m[0][0] =  cosf(tec3d_radians(theta));
	this->m[0][2] =  sinf(tec3d_radians(theta));
	this->m[3][0] = -sinf(tec3d_radians(theta));
	this->m[3][2] =  cosf(tec3d_radians(theta));
}

/*.............................................................................*/
// Rotate z
/*.............................................................................*/
GLvoid Matrix4x4::rotateZ(GLfloat theta) {
	
	this->m[0][0] =  cosf(tec3d_radians(theta));
	this->m[0][1] = -sinf(tec3d_radians(theta));
	this->m[1][0] =  sinf(tec3d_radians(theta));
	this->m[1][1] =  cosf(tec3d_radians(theta));
}

/*.............................................................................*/
// Scale with x, y, z
/*.............................................................................*/
GLvoid Matrix4x4::scale(GLfloat x, GLfloat y, GLfloat z) {
	
	this->scaleX(x);
	this->scaleY(y);
	this->scaleZ(z);
}

/*.............................................................................*/
// Scale with vector
/*.............................................................................*/
GLvoid Matrix4x4::scale(Vector3f vector) {
	
	this->scaleX(vector.x);
	this->scaleY(vector.y);
	this->scaleZ(vector.z);
}

/*.............................................................................*/
// Scale x
/*.............................................................................*/
GLvoid Matrix4x4::scaleX(GLfloat scale) {
	
	this->m[0][0] = scale;
}

/*.............................................................................*/
// Scale y
/*.............................................................................*/
GLvoid Matrix4x4::scaleY(GLfloat scale) {
	
	this->m[1][1] = scale;
}

/*.............................................................................*/
// Scale z
/*.............................................................................*/
GLvoid Matrix4x4::scaleZ(GLfloat scale) {
	
	this->m[2][2] = scale;
}

/*.............................................................................*/
// Translate with x, y, z
/*.............................................................................*/
GLvoid Matrix4x4::translate(GLfloat x, GLfloat y, GLfloat z) {
	
	this->translateX(x);
	this->translateY(y);
	this->translateZ(z);
}

/*.............................................................................*/
// Translate with vector
/*.............................................................................*/
GLvoid Matrix4x4::translate(Vector3f vector) {
	
	this->translateX(vector.x);
	this->translateY(vector.y);
	this->translateZ(vector.z);
}

/*.............................................................................*/
// Translate x
/*.............................................................................*/
GLvoid Matrix4x4::translateX(GLfloat value) {
	this->m[0][3] = value;
}

/*.............................................................................*/
// Translate y
/*.............................................................................*/
GLvoid Matrix4x4::translateY(GLfloat value) {
	this->m[0][3] = value;
}

/*.............................................................................*/
// Translate z
/*.............................................................................*/
GLvoid Matrix4x4::translateZ(GLfloat value) {
	this->m[0][3] = value;
}

/*.............................................................................*/
