 /******************************************************************************/
/*
 * tec3d_frag.frag
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 31/01/17.
 *
 */
/******************************************************************************/

// Default float precision
precision highp float;

// Ambient Light
float				ambientStrength;
uniform	highp vec3	ambientColor;

// Diffuse Light
float				diffuseStrength;
uniform	highp vec3	diffuseColor;

// Specular Light
float				specularStrength;
uniform highp vec3	specularColor;

// Material Opacity
uniform	float		opacity;

// Textures
uniform	sampler2D	ambient_texture;
uniform	sampler2D	diffuse_texture;
uniform	sampler2D	specular_texture;
uniform sampler2D	normal_texture;

// Vertex Shader properties
varying	highp vec3	vertexColor;
varying highp vec2	TexCoord;
varying highp vec3	norm;
varying highp vec3  lightDir;
varying highp vec3	camDir;
varying highp vec3  weight;

highp vec3			reflectDir;
float				diff;
float				spec;

/******************************************************************************/
// main
/******************************************************************************/
void main()
{
	ambientStrength		= 1.0;
	diffuseStrength		= 2.0;
	specularStrength	= 3.0;
	
	float minVary = 1.0;
	float maxVary = 2.0;
	
	// Calc normal
	highp vec4 normTex2D	= texture2D(normal_texture,TexCoord);
	highp vec3 normal		= normalize(norm + (normTex2D.rgb * maxVary - minVary));
	
	diff		= max(0.0,dot(lightDir, norm));
	reflectDir	= reflect(-lightDir, norm);
	spec		= pow(max(0.0,dot(camDir, reflectDir)),64.0);
	
	// Calc ambient
	highp vec4 ambiTex2D = texture2D(ambient_texture,TexCoord);
	highp vec4 ambient	= vec4(ambientColor,opacity) * ambientStrength;
	if (ambiTex2D.rgb != vec3(0.0,0.0,0.0)) {
		ambient *= ambiTex2D;
	}

	// Calc diffuse
	highp vec4 diffTex2D = texture2D(diffuse_texture,TexCoord);
	highp vec4 diffuse	 = diff * vec4(diffuseColor,opacity) * diffuseStrength;;
	if (diffTex2D.rgb != vec3(0.0,0.0,0.0)) {
		diffuse *= diffTex2D;
	}
	
	// Calc specular
	highp vec4 specTex2D = texture2D(specular_texture,TexCoord);
	highp vec4 specular	 = spec * vec4(1.0,1.0,1.0 ,opacity) * specularStrength;
	if (specTex2D.rgb != vec3(0.0,0.0,0.0)) {
		specular *= specTex2D;
	}
	
	//  ambient.rgb + diffuse.rgb + specular.rgb
	// vec4(ambient.rgb + diffuse.rgb + specular.rgb,1.0)
	// vec4(weight.xyz,1.0);//
	highp vec4 fragmentColor =  vec4(ambient.rgb + diffuse.rgb + specular.rgb,1.0);
	gl_FragColor = fragmentColor;
}
/******************************************************************************/
