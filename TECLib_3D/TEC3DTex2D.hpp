/******************************************************************************/
/*
 * TEC3DTex2D.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DTex2D_hpp
#define TEC3DTex2D_hpp

#include "TEC3DBase.h"

#ifdef __cplusplus

/******************************************************************************/
// TEC3DTex2D
/******************************************************************************/
namespace TEC3D {

	/**************************************************************************/
	// Tex2D
	/**************************************************************************/
	struct _Tex2D {
		// ID of texture
		GLuint id;
		// Type of texture
		std::string type;
		// Path of texture
		std::string path;
	};
	typedef _Tex2D Tex2D;
	
}


#endif

#endif /* TEC3DTex2D_hpp */
