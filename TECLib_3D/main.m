
//  main.m
//  TECLib_3D
//
//  Created by Cristiano Douglas on 25/01/17.
//  Copyright © 2017 Tecvidya Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
