/******************************************************************************/
 /* 
 * TEC3DModel.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#include "TEC3DModel.hpp"


using namespace TEC3D;


/*.............................................................................*/
// Constructor
/*.............................................................................*/
Model::Model(std::string dir, std::string name, Shader *shader) {
	this->shader = shader;
	this->file_dir = dir;
	this->file_path = dir + "/" + name;
	this->load(this->file_path);
	this->matrix = glm::mat4();
}

Model::Model(const GLvoid* buffer, size_t length) {
	this->load(buffer, length);
	
}

/*.............................................................................*/
// Destructor
/*.............................................................................*/
Model::~Model() {
	
}

/*.............................................................................*/
// Load model from memory
/*.............................................................................*/
GLvoid Model::load(const GLvoid* buffer, size_t length) {
	
	GLbyte* pBuffer = (GLbyte*)malloc(length);
	memcpy(pBuffer, buffer, length);
	
	this->m_scene = this->importer.ReadFileFromMemory(pBuffer, length, aiProcess_Triangulate | aiProcess_PreTransformVertices | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
	
	if (!this->m_scene || this->m_scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE ||
		!this->m_scene->mRootNode) {
		
		printf("**************************************************\n");
		printf("Model Error!\n");
		printf("Message: %s",importer.GetErrorString());
		printf("**************************************************\n");
		
		
	}
}

/*.............................................................................*/
// Load model from file
/*.............................................................................*/
GLvoid Model::load(const std::string name) {

	this->m_scene = this->importer.ReadFile(this->file_path, aiProcess_Triangulate |
											aiProcess_GenSmoothNormals |
											aiProcess_FlipUVs |
											aiProcess_JoinIdenticalVertices);
	
	FILE* file;
	file = fopen(this->file_path.c_str(), "r");
	if (file!=NULL) { printf("\n[ Model file Exist! ]\n");}
	
	if (!this->m_scene || this->m_scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE ||
		!this->m_scene->mRootNode) {
		
		printf("**************************************************\n");
		printf("Model Error!\n");
		printf("Message: %s\n",importer.GetErrorString());
		printf("**************************************************\n");
		
	} else {
	
		this->globalInverseTransform = this->m_scene->mRootNode->mTransformation;
		this->globalInverseTransform.Inverse();

		this->boneLocation = std::vector<uint>();
		this->boneInfo = std::vector<BoneInfo>();
		this->boneMapping = std::map<string,uint>();
		
		this->mesh = std::vector<Mesh>();
		
		this->numBones = 0;
		this->meshIndex = 0;
		this->boneLocations = GL_FALSE;

		this->entries.resize(this->m_scene->mNumMeshes);
		this->processNode(this->m_scene->mRootNode, this->m_scene);
		
	}
}

/*.............................................................................*/
// Init bone location in Shader program
/*.............................................................................*/
GLvoid Model::initBoneLocations(Shader* shader) {
	
	for (unsigned int i = 0 ; i < 100 ; i++) {
		char Name[128];
		memset(Name, 0, sizeof(Name));
		SNPRINTF(Name, sizeof(Name), "bones[%d]", i);
		GLuint location = glGetUniformLocation(this->shader->program,Name);
		this->boneLocation.push_back(location);
	}
	
}

/*.............................................................................*/
// Render
/*.............................................................................*/
GLvoid Model::play(double time) {
	
	this->m_startTime = time;
	this->playing = GL_TRUE;
}

/*.............................................................................*/
// Render
/*.............................................................................*/
GLvoid Model::render(double time) {
	
	if (!this->boneLocations) {
		initBoneLocations(shader);
		this->boneLocations = GL_TRUE;
	}
	
	float RunningTime = 30.0f;
	if ( this->playing )
		RunningTime =  (float)(time - m_startTime);
	
	vector<Matrix4f> Transforms;
	this->boneTransform(RunningTime, Transforms);
	
	for (uint i = 0 ; i < Transforms.size() ; i++) {
		Matrix4f transform = Transforms[i];
		
		glm::mat4x4 glmMatrix = glmMat4x4(transform);
		
		glUniformMatrix4fv(this->boneLocation[i], 1, GL_FALSE,glm::value_ptr(glmMatrix));
		
		if (!(getOESError(this,TEC3D_FUNCTION)==TEC3D_NO_ERROR)) {
			getGLSLProgramLog(shader);
			getGLSLShaderLog(shader, TEC3D_VERTEX);
		}
		
	}
	
	for (GLuint i=0; i<this->mesh.size(); i++) {
		//this->mesh[i].update(_transforms);
		//aiMeshAnim meshAnim = this->mesh
		this->mesh[i].render(this->matrix);
	}
}

/*.............................................................................*/
// Get a glm::mat4x4 from a Matrix4f
/*.............................................................................*/
glm::mat4x4 Model::glmMat4x4(Matrix4f _matrix) {
	glm::mat4x4 glmMatrix;
	
	glmMatrix[0][0] = _matrix.m[0][0];
	glmMatrix[0][1] = _matrix.m[0][1];
	glmMatrix[0][2] = _matrix.m[0][2];
	glmMatrix[0][3] = _matrix.m[0][3];
	
	glmMatrix[1][0] = _matrix.m[1][0];
	glmMatrix[1][1] = _matrix.m[1][1];
	glmMatrix[1][2] = _matrix.m[1][2];
	glmMatrix[1][3] = _matrix.m[1][3];
	
	glmMatrix[2][0] = _matrix.m[2][0];
	glmMatrix[2][1] = _matrix.m[2][1];
	glmMatrix[2][2] = _matrix.m[2][2];
	glmMatrix[2][3] = _matrix.m[2][3];
	
	glmMatrix[3][0] = _matrix.m[3][0];
	glmMatrix[3][1] = _matrix.m[3][1];
	glmMatrix[3][2] = _matrix.m[3][2];
	glmMatrix[3][3] = _matrix.m[3][3];
	
	return glmMatrix;
}

/*.............................................................................*/
// Process model node's
/*.............................................................................*/
GLvoid Model::processNode(aiNode* node, const aiScene* scene) {
	
	
	printf("......................\n");
	printf(" - NODE: %s\n",node->mName.C_Str());
	printf("......................\n");
	
	for (GLuint i=0; i<node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->entries.push_back(this->numVertices);
		this->numVertices += mesh->mNumVertices;
		this->mesh.push_back(this->processMesh(mesh, scene));

	}
	
	for (GLuint i=0; i<node->mNumChildren; i++) {
		aiNode* cNode = node->mChildren[i];
		this->processNode(cNode, scene);
		
	}
	
}

/*.............................................................................*/
// Process model mesh's
/*.............................................................................*/
Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene) {
	
	std::vector<Vertex> vertices = std::vector<Vertex>();
	std::vector<GLuint> indices = std::vector<GLuint>();
	std::vector<Tex2D>  textures = std::vector<Tex2D>();
	std::vector<VertexBoneData> bones = std::vector<VertexBoneData>();
	
	for (GLuint v=0; v<mesh->mNumVertices; v++) {
		
		Vertex vertex;
		aiVector3D* pos = &mesh->mVertices[v];
		vertex.pos = this->vector3fFrom(pos);
		
		if (mesh->mNormals!=NULL) {
			aiVector3D* norm = &mesh->mNormals[v];
			if (norm!=NULL) vertex.norm = this->vector3fFrom(norm);
		}
		
		aiVector3D* tex;
		
		if (mesh->mTextureCoords[0]) {
			tex = &mesh->mTextureCoords[0][v];
			vertex.texC = this->vector2fFrom(tex);
		} else {
			vertex.texC = {0.0f, 0.0f};
		}
		
		vertices.push_back(vertex);
	}
	
	for (GLuint f=0; f<mesh->mNumFaces;f++) {
		aiFace face = mesh->mFaces[f];
		for (GLuint i=0; i<face.mNumIndices;i++) {
			GLuint index = face.mIndices[i];
			indices.push_back(index);
		}
	}
	
	Material meshMaterial;
	
	aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
	if (material != NULL) {
		meshMaterial = this->getMaterialProperties(material);
		
		std::vector<Tex2D> diffuse = this->processMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuse.begin(), diffuse.end());
		
		// Load Specular Material
		std::vector<Tex2D> specular = this->processMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specular.begin(), specular.end());
		
		// Load normal Material
		std::vector<Tex2D> normals = this->processMaterialTextures(material, aiTextureType_HEIGHT, "texture_normals");
		textures.insert(textures.end(), normals.begin(), normals.end());
		
	} else {
		meshMaterial.ambientColor = {0.5f,0.5f,0.5f};
	}
	
	this->loadBones(mesh, &vertices);
	this->meshIndex += 1;
	
	Mesh _mesh(this->shader,vertices, indices, textures, meshMaterial);
	_mesh.name = std::string(mesh->mName.C_Str());
	return _mesh;
}


/*.............................................................................*/
// Process model material's
/*.............................................................................*/
std::vector<Tex2D> Model::processMaterial(aiMaterial* material) {
	

	std::vector<Tex2D> textures = std::vector<Tex2D>();
	
	std::vector<Tex2D> ambient = this->processMaterialTextures(material, aiTextureType_AMBIENT, "texture_ambient");
	textures.insert(textures.end(), ambient.begin(), ambient.end());
	
	// Load Difusse material
	std::vector<Tex2D> diffuse = this->processMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
	textures.insert(textures.end(), diffuse.begin(), diffuse.end());
	
	// Load Specular Material
	std::vector<Tex2D> specular = this->processMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
	textures.insert(textures.end(), specular.begin(), specular.end());
	
	// Load normal Material
	std::vector<Tex2D> normals = this->processMaterialTextures(material, aiTextureType_HEIGHT, "texture_normals");
	textures.insert(textures.end(), normals.begin(), normals.end());
	
	
	return textures;
	
}

/*.............................................................................*/
// Processs material textures
/*.............................................................................*/
std::vector<Tex2D> Model::processMaterialTextures(aiMaterial* mat, aiTextureType type,
										std::string typeName) {
	
	std::vector<Tex2D> textures;
	for (GLuint t=0; t<mat->GetTextureCount(type); t++) {
		aiString texStr;
		mat->GetTexture(type, t, &texStr);
		GLboolean exist = false;
		for (GLuint i=0; i<this->textures.size(); i++) {
			if (!strcmp(this->textures[i].path.c_str(), texStr.C_Str())) {
				textures.push_back(this->textures[i]);
				exist = true;
				break;
			}
		}
		if (!exist) {
			
			std::string name(texStr.C_Str());
			std::string path = this->file_dir + "/" + name;
			Tex2D texture;
			texture.id = TextureLoader::LoadTexture(path);
			texture.path = path;
			texture.type = typeName;
			textures.push_back(texture);
			this->textures.push_back(texture);
		}
	}
	
	return textures;
}


/*.............................................................................*/
// Load mesh bones
/*.............................................................................*/
GLvoid Model::loadBones(aiMesh* mesh, std::vector<Vertex>* vertices)
{

	
	for (uint i = 0 ; i < mesh->mNumBones ; i++) {
		uint BoneIndex = 0;
		string BoneName(mesh->mBones[i]->mName.data);
		
		if (this->boneMapping.find(BoneName) == this->boneMapping.end()) {
			
			BoneIndex = this->numBones;
			this->numBones++;
			BoneInfo bi;
			this->boneInfo.push_back(bi);
			this->boneInfo[BoneIndex].BoneOffset = mesh->mBones[i]->mOffsetMatrix;
			this->boneMapping[BoneName] = BoneIndex;
		}
		else {
			BoneIndex = this->boneMapping[BoneName];
		}
		
		for (uint j = 0 ; j < mesh->mBones[i]->mNumWeights ; j++) {
			
			uint VertexID = this->entries[this->meshIndex] + mesh->mBones[i]->mWeights[j].mVertexId;
			float Weight  = mesh->mBones[i]->mWeights[j].mWeight;
			(*vertices)[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}
	
}

/*.............................................................................*/
// Bone transform by animation time
/*.............................................................................*/
GLvoid Model::boneTransform(float TimeInSeconds, vector<Matrix4f>& Transforms)
{
	Matrix4f Identity;
	Identity.InitIdentity();
	
	float TicksPerSecond = (float)(this->m_scene->mAnimations[0]->mTicksPerSecond != 0 ? this->m_scene->mAnimations[0]->mTicksPerSecond : 25.0f);
	
	float TimeInTicks = TimeInSeconds * TicksPerSecond;
	float AnimationTime = fmod(TimeInTicks, (float)this->m_scene->mAnimations[0]->mDuration);
	
	this->readNodeHeirarchy(AnimationTime, this->m_scene->mRootNode, Identity);
	
	Transforms.resize(numBones);
	
	for (uint i = 0 ; i < numBones ; i++) {
		Transforms[i] = boneInfo[i].FinalTransformation;
	}
	
}

/*.............................................................................*/
// Read node hierarchy
/*.............................................................................*/
GLvoid Model::readNodeHeirarchy(float AnimationTime, const aiNode* pNode, const Matrix4f& ParentTransform)
{
	
	string NodeName(pNode->mName.data);
	
	const aiAnimation* pAnimation = this->m_scene->mAnimations[0];
	
	Matrix4f NodeTransformation(pNode->mTransformation);
	
	const aiNodeAnim* pNodeAnim = this->findNodeAnim(pAnimation, NodeName);
	
	if (pNodeAnim) {
		
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		this->calcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		Matrix4f ScalingM;
		ScalingM.InitScaleTransform(Scaling.x, Scaling.y, Scaling.z);
		//ScalingM = ScalingM.Transpose();
		
		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		this->calcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
		Matrix4f RotationM = Matrix4f(RotationQ.GetMatrix());
		//RotationM = RotationM.Transpose();
		
		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		this->calcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		Matrix4f TranslationM;
		TranslationM.InitTranslationTransform(Translation.x, Translation.y, Translation.z);
		//TranslationM = TranslationM.Transpose();
		
		// Combine the above transformations
		NodeTransformation = (TranslationM * RotationM * ScalingM);
	}
	
	Matrix4f GlobalTransformation = ParentTransform * NodeTransformation;
	
	if (this->boneMapping.find(NodeName) != this->boneMapping.end()) {
		uint BoneIndex = this->boneMapping[NodeName];
		
		this->boneInfo[BoneIndex].FinalTransformation = (this->globalInverseTransform * GlobalTransformation
														* this->boneInfo[BoneIndex].BoneOffset).Transpose();
	}
	
	for (uint i = 0 ; i < pNode->mNumChildren ; i++) {
		this->readNodeHeirarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	}
}

/*.............................................................................*/
// Find node animation
/*.............................................................................*/
const aiNodeAnim* Model::findNodeAnim(const aiAnimation* pAnimation, const string NodeName)
{
	for (uint i = 0 ; i < pAnimation->mNumChannels ; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];
		
		if (string(pNodeAnim->mNodeName.data) == NodeName) {
			return pNodeAnim;
		}
	}
	
	return NULL;
}

/*.............................................................................*/
// Find mesh animation
/*.............................................................................*/
const aiMeshAnim* Model::findMeshAnim(const aiAnimation* pAnimation, const string NodeName)
{
	for (uint i = 0 ; i < pAnimation->mNumMeshChannels; i++) {
		const aiMeshAnim* pMeshAnim = pAnimation->mMeshChannels[i];
		
		if (string(pMeshAnim->mName.data) == NodeName) {
			return pMeshAnim;
		}
	}
	
	return NULL;
}

/*.............................................................................*/
// Calculate interpolated position
/*.............................................................................*/
GLvoid Model::calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}
	
	uint PositionIndex = this->findPosition(AnimationTime, pNodeAnim);
	uint NextPositionIndex = (PositionIndex + 1);
	//assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime
							  - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	/*printf("*******************************************\n");
	for (int i=0; i < pNodeAnim->mNumPositionKeys; i++ ) {
		const aiVector3D& value = pNodeAnim->mPositionKeys[i].mValue;
		printf("x: %f y: %f z: %f\n",value.x,value.y,value.z);
	}
	printf("*******************************************\n");*/
	
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
	
	printf("");
}

/*.............................................................................*/
// Calculate interpolated rotation
/*.............................................................................*/
GLvoid Model::calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}
	
	uint RotationIndex = this->findRotation(AnimationTime, pNodeAnim);
	uint NextRotationIndex = (RotationIndex + 1);
	//assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime
							  - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ   = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
	return;
}

/*.............................................................................*/
// Calculate interpolated scaling
/*.............................................................................*/
GLvoid Model::calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}
	
	uint ScalingIndex = this->findScaling(AnimationTime, pNodeAnim);
	uint NextScalingIndex = (ScalingIndex + 1);
	//assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	
	/*printf("*******************************************\n");
	for (int i=0; i < pNodeAnim->mNumScalingKeys; i++ ) {
		const aiVector3D& value = pNodeAnim->mScalingKeys[i].mValue;
		printf("x: %f y: %f z: %f\n",value.x,value.y,value.z);
	}
	printf("*******************************************\n");*/
	
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End   = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

/*.............................................................................*/
// Find position of the animation
/*.............................................................................*/
uint Model::findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (uint i = 0 ; i < pNodeAnim->mNumPositionKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}
	
	//assert(0);
	
	return 0;
}

/*.............................................................................*/
// Find rotation of the animation
/*.............................................................................*/
uint Model::findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);
	
	for (uint i = 0 ; i < pNodeAnim->mNumRotationKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}
	
	//assert(0);
	
	return 0;
}

/*.............................................................................*/
// Find scaling of the animation
/*.............................................................................*/
uint Model::findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);
	
	for (uint i = 0 ; i < pNodeAnim->mNumScalingKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}
	
	//assert(0);
	
	return 0;
}


/*.............................................................................*/
// Get a Vector3f from aiVector3D
/*.............................................................................*/
Vector3f Model::vector3fFrom(aiVector3D* vector3D) {
	Vector3f vector = {vector3D->x,vector3D->y,vector3D->z}; return vector; }

/*.............................................................................*/
// Get a Vector2f from aiVector2D
/*.............................................................................*/
Vector2f Model::vector2fFrom(aiVector3D* vector3D) {
	Vector2f vector = {vector3D->x,vector3D->y}; return vector; }

/*.............................................................................*/
// Get all properties from material
/*.............................................................................*/
Material Model::getMaterialProperties(const aiMaterial* pMaterial)
{
	Material material;
	
	printf("-------------------------------------------------------------\n");
	aiString name;
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_NAME, name) )
	{
		std::string _name(name.data);
		printf("   Name: %s\n",name.data);
		material.name = _name;
	}
	aiColor3D color;
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_AMBIENT, color) )
	{
		Color3f _color;
		_color.r = color.r;
		_color.g = color.g;
		_color.b = color.b;
		material.ambientColor = _color;
		printf("   Ambient color: (%f, %f, %f)\n",color.r,color.g,color.b );
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color) )
	{
		Color3f _color;
		_color.r = color.r;
		_color.g = color.g;
		_color.b = color.b;
		material.diffuseColor = _color;
		printf("   Diffuse color: (%f, %f, %f)\n",color.r,color.g,color.b );
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_EMISSIVE, color) )
	{
		printf("   Emissive color: (%f, %f, %f)\n",color.r,color.g,color.b );
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_REFLECTIVE, color) )
	{
		printf("   Reflective color: (%f, %f, %f)\n",color.r,color.g,color.b );
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_SPECULAR, color) )
	{
		Color3f _color;
		_color.r = color.r;
		_color.g = color.g;
		_color.b = color.b;
		material.specularColor = _color;
		printf("   Specular color: (%f, %f, %f)\n",color.r,color.g,color.b );
	}
	float value;
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_SHININESS, value) )
	{
		printf("   Shininess: %f\n",value);
	}
	if ( AI_SUCCESS == pMaterial->Get(AI_MATKEY_OPACITY, value)) {
		printf("	Opacity: %f\n",value);
		material.opacity = value;
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, value) )
	{
		printf("   Shininess strength: %f\n",value);
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_COLOR_TRANSPARENT, color) )
	{
		printf("   Transparent color: (%f, %f, %f)\n",color.r,color.g,color.b );
	}
	int intValue;
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_ENABLE_WIREFRAME, intValue) )
	{
		if( intValue == 0 )
		{
			printf("   Wireframe: Disabled\n");
			material.wireframe = (GLuint)intValue;
		}
		else if( intValue == 1 )
		{
			material.wireframe = (GLuint)intValue;
			printf("   Wireframe: Enabled\n");
		}
		else
		{
			printf("   Wireframe: Unexpected value\n");
		}
	}
	if( AI_SUCCESS == pMaterial->Get(AI_MATKEY_SHADING_MODEL, intValue) )
	{
		printf("   Shading model: %d\n",intValue);
	}
	unsigned int aux = pMaterial->GetTextureCount(aiTextureType_AMBIENT);
	if( aux > 0 )
	{
		printf("   Number of ambient textures: %u\n",aux);
	}
	aux = pMaterial->GetTextureCount(aiTextureType_DIFFUSE);
	if( aux > 0 )
	{
		printf("   Number of diffuse textures: %u\n",aux);
	}
	
	aux = pMaterial->GetTextureCount(aiTextureType_SPECULAR);
	if( aux > 0 )
	{
		printf("   Number of specular textures: %u\n",aux);
	}
	
	aux = pMaterial->GetTextureCount(aiTextureType_NORMALS);
	if( aux > 0 )
	{

		printf("   Number of normals textures: %u\n",aux);
	}
	
	aux = pMaterial->GetTextureCount(aiTextureType_HEIGHT);
	if( aux > 0 )
	{
		
		printf("   Number of bump textures: %u\n",aux);
	}
	
	printf("-------------------------------------------------------------\n");
	return material;
}

/*.............................................................................*/
