/******************************************************************************/
/*
 * TEC3DMesh.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 26/01/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DMesh_hpp
#define TEC3DMesh_hpp

#include "TEC3DBase.h"

#include "TEC3DFoundation.h"
#include "TEC3DTex2D.hpp"
#include "TEC3DVertex.hpp"
#include "TEC3DEnum.h"
#include "TEC3DShader.hpp"

#include "TEC3DTextureLoader.h"
#include "TEC3DMaterial.hpp"


/******************************************************************************/
// TEC3DMesh
/******************************************************************************/
namespace TEC3D {
	
	/**************************************************************************/
	// BoneInfo
	/**************************************************************************/
	struct BoneInfo
	{
		Matrix4f BoneOffset;
		Matrix4f FinalTransformation;
		
		BoneInfo()
		{
			BoneOffset.SetZero();
			FinalTransformation.SetZero();
		}
	};
	
	/**************************************************************************/
	// VertexBoneData
	/**************************************************************************/
	struct VertexBoneData
	{
//		Vector4f IDs;
//		Vector4f Weights;
		
		float IDs[4];
		float Weights[4];
		
		VertexBoneData()
		{
			Reset();
		};
		
		void Reset()
		{
			/*Weights.x = 0.0f;
			Weights.y = 0.0f;
			Weights.z = 0.0f;
			Weights.w = 0.0f;
			
			IDs.x = 0.0f;
			IDs.y = 0.0f;
			IDs.z = 0.0f;
			IDs.w = 0.0f;*/
			ZERO_MEM(IDs);
			ZERO_MEM(Weights);
		}
		
		void AddBoneData(uint BoneID, float Weight)
		{
			/*if (Weights.x == 0.0) {
				Weights.x = Weight;
				IDs.x	= BoneID;
				return;
			}
			
			if (Weights.y == 0.0) {
				Weights.y = Weight;
				IDs.y	= BoneID;
				return;
			}
			
			if (Weights.z == 0.0) {
				Weights.z = Weight;
				IDs.z	= BoneID;
				return;
			}
			
			if (Weights.w == 0.0) {
				Weights.w = Weight;
				IDs.w	= BoneID;
				return;
			}*/
			
			for (uint i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(IDs) ; i++) {
				if (Weights[i] == 0.0) {
					IDs[i]     = (float)BoneID;
					Weights[i] = Weight;
					return;
				}
			}
			
			//assert(0);
		}

	};
	
	
	/**************************************************************************/
	// Mesh
	/**************************************************************************/
	class Mesh {
		
	/*........................................................................*/
	// Public properties
	/*........................................................................*/
	public:
		
		std::vector<Vertex>		vertex;
		std::vector<GLuint>		index;
		std::vector<Tex2D>		texture;
		Material				material;
		glm::mat4x4				localMatrix;
		glm::mat4x4				globalMatrix;
		std::string					name;
		
	/*........................................................................*/
	// Private properties
	/*........................................................................*/
	private:
		
		GLuint	vao;
		GLuint	vbo;
		GLuint	ebo;
		GLuint  bvb;
		Shader *shader;
		
	/*........................................................................*/
	// Public method's
	/*........................................................................*/
	public:
		
		Mesh();
		Mesh(Shader *shader, std::vector<Vertex> vertex, std::vector<GLuint> index,
			 std::vector<Tex2D> texture, Material material);
		
		void render(glm::mat4 matrix);
		void update(vector<glm::mat4x4> transforms);
		
	/*........................................................................*/
	// Private method's
	/*........................................................................*/
	private:
		
		void prepare();
		
	};
	
}
/******************************************************************************/


#endif /* TEC3DMesh_hpp */
