/******************************************************************************/
/*
 * TEC3DTextureLoader.mm
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 31/01/17.
 *
 */
/******************************************************************************/

#import "TEC3DTextureLoader.h"


using namespace TEC3D;

/*.............................................................................*/
// Load a texture and return OpenGL texture id
/*.............................................................................*/
GLuint TextureLoader::LoadTexture(std::string imagefile)
{
	
	NSString *path = [[NSString alloc] initWithUTF8String:imagefile.c_str()];
	path = [path stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
	printf("***************************************************\n");
	printf("Load texture: %s\n",[path cStringUsingEncoding:NSUTF8StringEncoding]);
	
	NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
	UIImage *image = [[UIImage alloc] initWithData:texData];
	if (image == nil) {
		printf("FAIL!\n");
		return 0;
	}
	
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	GLuint width = (GLuint)CGImageGetWidth(image.CGImage);
	GLuint height = (GLuint)CGImageGetHeight(image.CGImage);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	void *imageData = malloc( height * width * 4 );
	CGContextRef imgcontext = CGBitmapContextCreate( imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
	CGColorSpaceRelease( colorSpace );
	CGContextClearRect( imgcontext, CGRectMake( 0, 0, width, height ) );
	CGContextTranslateCTM( imgcontext, 0, height - height );
	CGContextDrawImage( imgcontext, CGRectMake( 0, 0, width, height ), image.CGImage );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	glBindTexture(GL_TEXTURE_2D, 0);
	CGContextRelease(imgcontext);
	free(imageData);
	
	printf("Texture load SUCCESS!\n");
	printf("***************************************************\n");
	
	getOESError(TEC3D_FUNCTION);
	
	return texture;
}

/*.............................................................................*/
// Load a texture from a Tex2D pointer and return error
/*.............................................................................*/
GLuint TextureLoader::LoadTexture(Tex2D* texture) {
	
	NSString *path = [[NSString alloc] initWithUTF8String:texture->path.c_str()];
	path = [path stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
	printf("***************************************************\n");
	printf("Load texture: %s\n",[path cStringUsingEncoding:NSUTF8StringEncoding]);
	
	NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
	UIImage *image = [[UIImage alloc] initWithData:texData];
	if (image == nil) {
		printf("FAIL!\n");
		return 1;
	}
	
	glGenTextures(1, &texture->id);
	glBindTexture(GL_TEXTURE_2D, texture->id);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	GLuint width = (GLuint)CGImageGetWidth(image.CGImage);
	GLuint height = (GLuint)CGImageGetHeight(image.CGImage);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	void *imageData = malloc( height * width * 4 );
	CGContextRef imgcontext = CGBitmapContextCreate( imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
	CGColorSpaceRelease( colorSpace );
	CGContextClearRect( imgcontext, CGRectMake( 0, 0, width, height ) );
	CGContextTranslateCTM( imgcontext, 0, height - height );
	CGContextDrawImage( imgcontext, CGRectMake( 0, 0, width, height ), image.CGImage );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	glBindTexture(GL_TEXTURE_2D, 0);
	CGContextRelease(imgcontext);
	free(imageData);
	
	printf("Texture load SUCCESS!\n");
	printf("***************************************************\n");
	
	getOESError(TEC3D_FUNCTION);
	
	return 0;
}

