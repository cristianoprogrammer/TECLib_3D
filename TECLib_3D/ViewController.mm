//
//  ViewController.m
//  TECLib_3D
//
//  Created by Cristiano Douglas on 25/01/17.
//  Copyright © 2017 Tecvidya Solutions. All rights reserved.
//

#import "ViewController.h"

#import "TEC3DFoundation.h"
#import "TEC3DModel.hpp"
#import "TEC3DCore.h"
#import "TEC3DShader.hpp"
#import "TEC3DRender.hpp"
#import "TEC3DSurface.hpp"
#import "mesh.h"

#import <QuartzCore/QuartzCore.h>

#import "GPUImage.h"

@interface ViewController ()
{
	IBOutlet GLKView* glView;
//	EAGLContext* eaglContext;
	CAEAGLLayer* eaglLayer;
	
	TEC3DAppleRenderHandler* renderHandler;
	
	TEC3D::Render* render;
	TEC3D::Shader* shader;
	TEC3D::Model* model;
	
	TEC3D::Surface* backSurface;
	TEC3D::Surface* thumbSurface;
	
	Mesh* mesh;
	
	GLfloat lastScale;
	GLfloat scale;
	GLfloat realScale;
	GLfloat angle;
	GLfloat langle;
	
	CGPoint touchPoint;
	CGPoint lastTouchPoint;
	CGPoint glPoint;
	CGPoint glTransPoint;
	
	GLuint uiVBO[4];
	GLuint uiVAO;
	GLuint uiEBO;
	
	GPUImageVideoCamera			*videoCamera;
	
	CVOpenGLESTextureCacheRef	textureCache;
	CVOpenGLESTextureRef		textureFrameBuffer;
	CVPixelBufferRef			pixelFrameBuffer;
	CGSize						glTextureSize;
	CGSize						glFrameBufferSize;
	GLuint						frameBuffer;
	GLuint						texTarget;
	GLuint						texId;
	GLuint						texFrameBufferId;
	
	AVAudioPlayer				*audioPlayer;
	
	__weak IBOutlet UIImageView *glImageView;
	
	__weak IBOutlet UISlider *sliderX;
	
	__weak IBOutlet UISlider *sliderY;
	
	__weak IBOutlet UISlider *sliderZ;
	
	BOOL firstFrame;
	
}


@end

@implementation ViewController 

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	self->firstFrame = YES;
//	self->glTextureSize = CGSizeMake(512.0f,512.0f);
//	self->glFrameBufferSize = CGSizeMake(480.0f, 640.0f);
	
//	self->eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:[[[GPUImageContext sharedImageProcessingContext] context] sharegroup]];
//	[EAGLContext setCurrentContext:self->eaglContext];
//	self->glView.context = self->eaglContext;
//	self->glView.delegate = self;
//	self->glView.enableSetNeedsDisplay = YES;
//	
//	eaglLayer = (CAEAGLLayer*) self->glView.layer;
//	eaglLayer.opaque = NO;
//	eaglLayer.drawableProperties =
//	[NSDictionary dictionaryWithObjectsAndKeys: kEAGLColorFormatRGBA8,
//		kEAGLDrawablePropertyColorFormat, nil];
//	
//	// Control gl update
//	CADisplayLink* displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(render:)];	[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	
	NSURL* audioURL  = [[NSBundle mainBundle] URLForResource:@"cs" withExtension:@"mp3"];
	self->audioPlayer = [[AVAudioPlayer alloc]  initWithContentsOfURL:audioURL error:nil];
	[self->audioPlayer setNumberOfLoops:-1];
	
	self->renderHandler = [[TEC3DAppleRenderHandler alloc] initWithGLKView:self->glView];
	[self->renderHandler setDelegate:self];
	[self->renderHandler prepareToMulticamera];
	
	[self->renderHandler setRenderTargetDelegate:self];
	[self->renderHandler prepareToRenderTarget: CGSizeMake(480.0f, 640.0f)];
	//[self->renderHandler setImageView:self->glImageView];
	[self->glImageView setHidden:YES];
	
	// Init OpenGL
	[self initGL];
	
	// Init model
	NSString* dir = [[NSBundle mainBundle] resourcePath];
	std::string c_dir([dir fileSystemRepresentation]);
	self->model = new TEC3D::Model(c_dir,"Lynx_Matador.FBX",self->shader);
	
	//self->mesh = new Mesh();
	//self->mesh->load(c_dir, "rebolativa02.FBX");
	
	
	[self configFrameBuffer];
	[self configGPU];
	
	// Config zoom gesture
	self->lastScale = 0.0f;
	self->scale = 0.0f;
	self->realScale = 0.43;
	self->glTransPoint = CGPointMake(-5.0f,
									 -19.12);
	UIPinchGestureRecognizer* gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
	[self->glView addGestureRecognizer:gesture];
	
	UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
	[self->glView addGestureRecognizer:panGesture];
	
	sliderX.hidden = YES;
	sliderZ.hidden = YES;
	sliderY.hidden = YES;
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self->audioPlayer play];
	[self->renderHandler start];
	
	self->model->play(CACurrentMediaTime());
	
	
}

- (void) configFrameBuffer {
	
	glGenFramebuffers(1, &self->frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, self->frameBuffer);
	
	CFDictionaryRef empty;
	CFMutableDictionaryRef attrs;
	
	empty = CFDictionaryCreate(kCFAllocatorDefault,
							   NULL,
							   NULL,
							   0,
							   &kCFTypeDictionaryKeyCallBacks,
							   &kCFTypeDictionaryValueCallBacks);
	
	attrs = CFDictionaryCreateMutable(kCFAllocatorDefault,
									  1,
									  &kCFTypeDictionaryKeyCallBacks,
									  &kCFTypeDictionaryValueCallBacks);
	
	CFDictionarySetValue(attrs,
						 kCVPixelBufferIOSurfacePropertiesKey,
						 empty);
	
	CVReturn error = CVPixelBufferCreate ( kCFAllocatorDefault, self->glFrameBufferSize.width , self->glFrameBufferSize.height, kCVPixelFormatType_32BGRA,
										  attrs,
										  &self->pixelFrameBuffer ) ;
	
	if(error != kCVReturnSuccess || self->pixelFrameBuffer == NULL) {
		NSLog(@"TEC3D CVPixelBufferRef create error: %d",error);
		return ;
	}

	CVOpenGLESTextureCacheRef coreVideoTextureCache = NULL;
	CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, [TEC3DAppleContext sharedContextGPUSharedGroup], NULL, &coreVideoTextureCache);

	error = CVOpenGLESTextureCacheCreateTextureFromImage (
														  kCFAllocatorDefault,
														  coreVideoTextureCache,
														  self->pixelFrameBuffer,
														  NULL,
														  GL_TEXTURE_2D,
														  GL_RGBA,
														  self->glFrameBufferSize.width,
														  self->glFrameBufferSize.height,
														  GL_BGRA,
														  GL_UNSIGNED_BYTE,
														  0,
														  &self->textureFrameBuffer);
	
	
	if (error != kCVReturnSuccess) {
		NSLog(@"TEC3D Error on create texture from image: %d",error);
		return ;
	}
	
	self->texFrameBufferId = CVOpenGLESTextureGetName(self->textureFrameBuffer);
	self->texTarget = CVOpenGLESTextureGetTarget(self->textureFrameBuffer);
	glBindTexture(GL_TEXTURE_2D, self->texFrameBufferId);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glBindFramebuffer(GL_FRAMEBUFFER, self->frameBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
						   GL_TEXTURE_2D, self->texFrameBufferId, 0);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//CVOpenGLESTextureCacheFlush(coreVideoTextureCache, 0);
	
	CFRelease(attrs);
	CFRelease(empty);
	CFRelease(coreVideoTextureCache);
	
}

- (void) configGPU {
	
	self->videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPresetiFrame960x540 cameraPosition:AVCaptureDevicePositionBack];
	self->videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
	//self->videoCamera.horizontallyMirrorFrontFacingCamera = NO;
	
	GPUImageView *filteredVideoView = [[GPUImageView alloc] initWithFrame:(CGRect){0, 0, self.view.bounds.size.width, self.view.bounds.size.height}];
	
	GPUImageRawDataOutput *rawDataOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:CGSizeMake(480, 640) resultsInBGRAFormat:YES];
	[self->videoCamera addTarget:(GPUImageView*)self.view];
	
	
	
//	GPUImageLowPassFilter* filter = [[GPUImageLowPassFilter alloc] init];
//	[self->videoCamera addTarget:filter];
//	[filter addTarget:(GPUImageView*)self.view];
	
	//glGenTextures(1, &self->texId);
	//self->texTarget = GL_TEXTURE_2D;
	
	//[_filter addTarget:rawDataOutput];
	/*[self->videoCamera addTarget:rawDataOutput];
	__weak GPUImageRawDataOutput *weakOutput = rawDataOutput;
	__weak typeof(self) wself = self;
	[rawDataOutput setNewFrameAvailableBlock:^{
		__strong GPUImageRawDataOutput *strongOutput = weakOutput;
		__strong typeof(wself) _self = wself;
		[strongOutput lockFramebufferForReading];
		GLubyte *outputBytes = [strongOutput rawBytesForImage];
		NSInteger bytesPerRow = [strongOutput bytesPerRowInOutput];
		
		
		CVPixelBufferRef pixelBuffer;
		CVPixelBufferCreateWithBytes(kCFAllocatorDefault,480, 640, kCVPixelFormatType_32BGRA, outputBytes, bytesPerRow, nil, nil, nil, &pixelBuffer);
		int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
		int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
		bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer);
		
		CVPixelBufferLockBaseAddress(pixelBuffer, 0);
		uint8_t *copyBaseAddress = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
		memcpy(copyBaseAddress, baseAddress, bufferHeight * bytesPerRow);
		
		CVPixelBufferLockBaseAddress(pixelBuffer, 0);
	
		GLuint width = (GLuint)CVPixelBufferGetWidth(pixelBuffer);
		GLuint height = (GLuint)CVPixelBufferGetHeight(pixelBuffer);
		
		CVOpenGLESTextureRef texture;
		
		//self->backSurface->setTextureFromBytes(outputBytes, GL_BGRA);
		//CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBuffer);
		//[self->renderHandler setMainCamImageFromBytes:outputBytes width: bufferWidth height:bufferHeight];
		//[self->renderHandler prepareForRender];
		//[self->renderHandler setSecondCamImageFromBytes:outputBytes  width: bufferWidth height:bufferHeight];
		
		[strongOutput unlockFramebufferAfterReading];

	}];*/
	[self->videoCamera startCameraCapture];
	
}


- (void)handlePanGesture:(UIPanGestureRecognizer *) sender {
	
	if (sender.state == UIGestureRecognizerStateBegan) {
		self->lastTouchPoint = [sender locationInView:self->glView];
	}
	
	self->touchPoint = [sender locationInView:self->glView];
	float x = (self->touchPoint.x - self->lastTouchPoint.x)/(self->glView.bounds.size.width)/2;
	float y = (self->touchPoint.y - self->lastTouchPoint.y)/(self->glView.bounds.size.height)/2;
	self->glPoint = CGPointMake(x, y);
	
//	self->touchPoint = [sender locationInView:self->glView];
//	angle = ((self->touchPoint.x - self->lastTouchPoint.x) / (self->glView.bounds.size.width / 360.0f)) * 3.1415927f / 180.0f;
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)sender {
	
	if (sender.state == UIGestureRecognizerStateBegan) {
		self->lastScale = sender.scale;
	}
	
	if (sender.scale > lastScale) {
		//NSLog(@"Scale: %f",sender.scale);
		self->scale = (sender.scale - lastScale)/8;
	} else if (sender.scale < lastScale) {
		//NSLog(@"Scale: %f",sender.scale);
		self->scale = (sender.scale - lastScale)/2;
	}
	
	self->lastScale = sender.scale;

}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - CoreAnimation

- (void)render:(CADisplayLink*)displayLink {
	[self->glView display];
}

#pragma mark - OpenGLES


// Initialize OpenGLES system
- (void) initGL {
	
	self->render = new TEC3D::Render();
	[self initShader];
	
	//self->render->attachShader(self->shader);
	
	//self->texID = TEC3D::TextureLoader::LoadTexture(c_dir + "/" + "texture.jpg");
}

// Initalize TEC3DShader
- (void) initShader {
	
	NSString* fragPath = [[NSBundle mainBundle] pathForResource:@"tec3d_frag" ofType:@"frag"];
	NSString* vertPath = [[NSBundle mainBundle] pathForResource:@"tec3d_vert" ofType:@"vert"];
	
	NSData* fragData = [[NSData alloc] initWithContentsOfFile:fragPath];
	NSData* vertData = [[NSData alloc] initWithContentsOfFile:vertPath];
	
	NSString* fragString	= [[NSString alloc] initWithData:fragData encoding:NSUTF8StringEncoding];
	NSString* vertString	= [[NSString alloc] initWithData:vertData encoding:NSUTF8StringEncoding];
	
	const char* fragmentC	= [fragString cStringUsingEncoding:NSUTF8StringEncoding];
	const char* vertexC		= [vertString cStringUsingEncoding:NSUTF8StringEncoding];
	
	std::string fragmentShader(fragmentC);
	std::string vertexShader(vertexC);
	
	self->shader = new TEC3D::Shader(vertexShader,fragmentShader);
	self->render->attachShader(self->shader);

}

// Draw model
- (void) renderScene {
	//self->render->setPerspective();
	self->realScale = self->realScale + self->scale;
	self->scale = 0.0f;
	if (self->realScale < 0.001f) self->realScale = 0.001f;
	if (self->realScale > 50.0f) self->realScale = 50.0f;

	self->glTransPoint = CGPointMake(self->glTransPoint.x + self->glPoint.x,
									 self->glTransPoint.y - self->glPoint.y);
	self->glPoint = CGPointMake(glTransPoint.x, glTransPoint.y);
	//NSLog(@"Scale: %f",self->realScale);
	//NSLog(@"Angle: %f",self->angle);
	//NSLog(@"Position: %f,%f",self->glTransPoint.x,self->glTransPoint.y);
	
	//angle =  (90.0/180.0*3.1415927);
	self->realScale = 4.5f;
	
//	self->realScale = 8.0f; //Zombie
	
//	angle = 0.01; //Zombie
	angle = 3.14;
	
	self->model->matrix = glm::mat4();
	
	// Translate
	self->model->matrix = glm::translate(self->model->matrix,
										 glm::vec3(-10,
												   -140.0,
												   -150.0f));
	
//	Zombie
//	self->model->matrix = glm::translate(self->model->matrix,
//										 glm::vec3(-70,
//												   -400.0,
//												   -150.0f));
	
	// Rotate
	self->model->matrix = glm::rotate(self->model->matrix,
									  angle, glm::vec3(0.0f,1.0f,0.0f));
	// Scale
	self->model->matrix = glm::scale(self->model->matrix,
									 glm::vec3(self->realScale,
											   self->realScale,
											   self->realScale));
	self->model->render(CACurrentMediaTime());
	//self->mesh->Render(self->shader);
}

- (void) tec3d_render_result:(CVPixelBufferRef) pixelBuffer {
	
	// Get the number of bytes per row for the pixel buffer
	/*uint8_t* baseAddress = (uint8_t*) CVPixelBufferGetBaseAddress(pixelBuffer);
	int width = (int)CVPixelBufferGetWidth(pixelBuffer);
	int height = (int)CVPixelBufferGetHeight(pixelBuffer);
	// Create a device-dependent RGB color space
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// Create a bitmap graphics context with the sample buffer data
	CGContextRef context = CGBitmapContextCreate(
												 baseAddress, width, height, 8, CVPixelBufferGetBytesPerRow(pixelBuffer),
												 colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
	
	
	// Create a Quartz image from the pixel data in the bitmap graphics context
	CGImageRef quartzImage = CGBitmapContextCreateImage(context);
	// Unlock the pixel buffer
	CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
	
	// Free up the context and color space
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
	
	// Create an image object from the Quartz image
	UIImage *image = [UIImage imageWithCGImage:quartzImage];
	
	// Release the Quartz image
	CGImageRelease(quartzImage);
	
	//captureImage.hidden = FALSE;
	[glImageView performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];*/
	
}

// GKL Draw
- (void) tec3d_render {
	
	
	//glBindFramebuffer(GL_FRAMEBUFFER, self->frameBuffer);
	//glViewport(0, 0, self->glFrameBufferSize.width, self->glFrameBufferSize.height);

	self->render->setClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	self->shader->useProgram();
	self->render->setPerspective();
	self->render->clear();
	
//	glDisable(GL_DEPTH_TEST);
	
	
	// Background surface
	/*glm::mat4 matrix = glm::mat4();
	matrix = glm::scale(matrix, glm::vec3(4.3f,5.6f,1.0f));
	matrix = glm::translate(matrix, glm::vec3(0.0f,0.0f,-10.0f));
	matrix = glm::rotate(matrix, 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	GLint modelMatrix = glGetUniformLocation(shader->program, "modelViewMatrix");
	glUniformMatrix4fv(modelMatrix, 1, GL_FALSE, glm::value_ptr(matrix));
	GLint diffuseColorL = glGetUniformLocation(shader->program, "ambientColor");
	glUniform3f(diffuseColorL, 0.5f, 0.5f, 0.5f);
	glActiveTexture(GL_TEXTURE0);
	glUniform1f(glGetUniformLocation(shader->program, "ambient_texture"), 0);
	glBindTexture(GL_TEXTURE_2D,
				  self->texId);
	glBindVertexArray(uiVAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(0);
	
	// Thumb surface
	glm::mat4 matrixThumb = glm::mat4();
	matrixThumb = glm::scale(matrixThumb, glm::vec3(1.0f,1.0f,1.0f));
	matrixThumb = glm::translate(matrixThumb, glm::vec3(-0.8f,-1.4f,-5.0f));
	matrixThumb = glm::rotate(matrixThumb, 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(modelMatrix, 1, GL_FALSE, glm::value_ptr(matrixThumb));
	glUniform3f(diffuseColorL, 1.0f, 1.0f, 1.0f);
	glActiveTexture(GL_TEXTURE0);
	glUniform1f(glGetUniformLocation(shader->program, "ambient_texture"), 0);
	glBindTexture(GL_TEXTURE_2D,
				  self->texId);
	glBindVertexArray(uiVAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(0);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClear(GL_DEPTH_BUFFER_BIT);*/
	
	//self->render->setPerspective();
	[self renderScene];
	
	//glFlush();
	//self->render->setOrthographic();
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//[self->eaglContext presentRenderbuffer:self->frameBuffer];
}


@end
