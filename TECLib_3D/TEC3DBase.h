//
//  TEC3DBase.h
//  TECLib_3D
//
//  Created by Cristiano Douglas on 26/01/17.
//  Copyright © 2017 Tecvidya Solutions. All rights reserved.
//

#ifndef TEC3DBase_h
#define TEC3DBase_h

#include <OpenGLES/ES3/gl.h>

#include <stdio.h>
#include <math.h>

#ifdef __cplusplus
#include <string>
#include <vector>
#include <map>
#include <chrono>
#endif


#include "TEC3DCore.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "ogldev_math_3d.h"



#ifndef TEC3D_FUNCTION
#define TEC3D_FUNCTION __FUNCTION__
#endif

#ifndef TEC3D_NO_ERROR
#define TEC3D_NO_ERROR	0x1001
#endif

#ifndef TEC3D_ERROR
#define TEC3D_ERROR		0x1002
#endif

#ifndef TEC3D_VERTEX
#define TEC3D_VERTEX	0x1101
#endif

#ifndef TEC3D_FRAGMENT
#define TEC3D_FRAGMENT	0x1102
#endif

#ifndef TEC3D_VERTEX_STRING
#define TEC3D_VERTEX_STRING "VERTEX_SHADER"
#endif


#ifndef TEC3D_FRAGMENT_STRING
#define TEC3D_FRAGMENT_STRING "FRAGMENT_SHADER"
#endif


#endif /* TEC3DBase_h */
