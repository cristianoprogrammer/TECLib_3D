/******************************************************************************/
/*
 * TEC3DRenderTarget.hpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 15/03/17.
 *
 */
/******************************************************************************/

#ifndef TEC3DRenderTarget_hpp
#define TEC3DRenderTarget_hpp

#include "TEC3DBase.h"


/******************************************************************************/
// TEC3DModel
/******************************************************************************/
namespace TEC3D {
	
	/**************************************************************************/
	// Render Target
	/**************************************************************************/
	class RenderTarget {
		
	/*........................................................................*/
	// Private properties
	/*........................................................................*/
	private:
		
		GLuint						frameBuffer;
		GLuint						texFrameBufferId;
		
		float						width;
		float						height;
		
	};
	
}

#endif /* TEC3DRenderTarget_hpp */
