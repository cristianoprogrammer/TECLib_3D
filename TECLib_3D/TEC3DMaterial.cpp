/******************************************************************************/
/*
 * TEC3DMaterial.cpp
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 13/02/17.
 *
 */
/******************************************************************************/


#include "TEC3DMaterial.hpp"


using namespace TEC3D;

Material::Material() {
	
	this->wireframe = 0;
	this->opacity	= 1.0f;
	
}
