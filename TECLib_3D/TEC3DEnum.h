/******************************************************************************/
/*
 * TEC3DEnum.h
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 25/01/17.
 *
 */
/******************************************************************************/

#ifndef TECEnum_h
#define TECEnum_h

/******************************************************************************/


/*............................................................................*/
// TECVectexAttrib
/*............................................................................*/

// Data index in GPU buffer
enum _TEC3DVertexAttrib
{
	TEC3DVertexAttribPosition,
	TEC3DVertexAttribNormal,
	TEC3DVertexAttribTexCoord,
	TEC3DVertexAttribBoneId,
	TEC3DVertexAttribBoneWeight
};
typedef _TEC3DVertexAttrib TEC3DVertexAttrib;

/******************************************************************************/

#endif /* TECEnum_h */
