/******************************************************************************/
/*
 * TEC3DAppleRenderHandler.h
 * TECLib_3D
 *
 * _____________________________________________________________________________
 *
 * Copyright © 2017 Tecvidya Solutions
 * All Rights Reserved
 * _____________________________________________________________________________
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by by Cristiano Douglas<cristiano@tecvidya.com.br> on 16/03/17.
 *
 */
/******************************************************************************/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <GLKit/GLKit.h>
#import <OpenGLES/EAGL.h>
#import <QuartzCore/QuartzCore.h>

#import "TEC3DAppleContext.h"

#ifdef __cplusplus
#import "TEC3DRender.hpp"
#import "TEC3DSurface.hpp"
#endif

/******************************************************************************/
@protocol TEC3DAppleRenderDelegate <NSObject>

- (void) tec3d_render;

@end
/******************************************************************************/
@protocol TEC3DAppleRenderTargetDelegate <NSObject>

- (void) tec3d_render_result:(CVPixelBufferRef) pixelBuffer;

@end
/******************************************************************************/

/******************************************************************************/
// TEC3DAppleRenderHandler
/******************************************************************************/
@interface TEC3DAppleRenderHandler : NSObject <GLKViewDelegate>
{
	/*........................................................................*/
	// GLKit and OES
	
	id<TEC3DAppleRenderDelegate> m_delegate;
	GLKView*		m_glkView;
	CADisplayLink*	m_displayLink;
	CAEAGLLayer*	m_eaglLayer;
	
	/*........................................................................*/
	// Render
	TEC3D::Render*	m_render;
	BOOL			m_defaultRenderEnabled;
	TEC3D::Shader*	m_defaultShader;
	
	/*........................................................................*/
	// Rende Target
	
	id<TEC3DAppleRenderTargetDelegate> m_renderTargetDelegate;
	BOOL			m_renderTargetEnabled;
	
	CVOpenGLESTextureRef		m_fbTextureRef;
	CVPixelBufferRef			m_fbPixelBuffer;
	CGSize						m_fbSize;
	GLuint						m_frameBufferID;
	GLuint						m_fbTarget;
	GLuint						m_fbTextureID;
	GLuint						m_renderBufferID;
	GLuint						m_depthRenderBufferID;
	TEC3D::Surface*				m_renderSurface;
	UIImageView*				m_imageView;
	
	/*........................................................................*/
	// Multicam
	
	TEC3D::Surface* m_mainCamSurface;
	TEC3D::Surface* m_secondCamSurface;
	BOOL	m_multicameraEnabled;
	
	/*........................................................................*/
}
/*............................................................................*/

- (id)		initWithGLKView:(GLKView*)glkView;
- (id)		initWithGLKView:(GLKView*)glkView render:(TEC3D::Render*) render;
- (void)	setDelegate:(id<TEC3DAppleRenderDelegate>)delegate;
- (void)	prepareForRender;

- (void)	prepareToRenderTarget:(CGSize)resultSize;
- (void)	setRenderTargetDelegate:(id<TEC3DAppleRenderTargetDelegate>)delegate;
- (void)	setImageView:(UIImageView*) imageView;

- (void)	prepareToMulticamera;
- (void)	setMainCamImageFromBytes:(GLvoid*) data width:(NSInteger)width height:(NSInteger)height;
- (void)	setSecondCamImageFromBytes:(GLvoid*) data width:(NSInteger)width height:(NSInteger)height;

- (void)	start;

/*............................................................................*/
@end
/******************************************************************************/
